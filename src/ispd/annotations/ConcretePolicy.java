package ispd.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * An annotation intended for concrete subclasses of the {@link ispd.policy.Policy Policy} abstract
 * class.
 * <hr>
 * <p>
 * Currently, it only serves as a way to remove warnings about unused target classes (which are only
 * used via reflection); In the future, however, it may be used as a means to reflectively lookup
 * instantiatable policies.
 * <p>
 * If that change is implemented, this policy's {@link Retention} must be changed to
 * {@link RetentionPolicy#RUNTIME RUNTIME}. Furthermore, fields may be added to hold useful
 * information about the policies, such as in which version they were specified (to aid with
 * backwards compatibility) and their symbolic name (to permit very detailed class names and simple
 * names to display in the UI).
 */
@Retention(RetentionPolicy.SOURCE)
public @interface ConcretePolicy {
}
