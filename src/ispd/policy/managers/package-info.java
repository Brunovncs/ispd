/**
 * This package contains classes that provide methods for reading, saving, compiling and importing
 * {@link ispd.policy.Policy Policy} implementations, as defined by the
 * {@link ispd.policy.PolicyManager PolicyManager} interface.
 *
 * <hr>
 * <p>
 * The {@link ispd.policy.managers.FilePolicyManager FilePolicyManager} class serves as an abstract
 * class for concrete policy manager classes that use filesystem-based storage. It has three
 * concrete implementations, namely
 * {@link ispd.policy.managers.GridSchedulingPolicyManager GridSchedulingPolicyManager},
 * {@link ispd.policy.managers.CloudSchedulingPolicyManager CloudSchedulingPolicyManager} and
 * {@link ispd.policy.managers.VmAllocationPolicyManager VmAllocationPolicyManager}, which all
 * specialize the abstract class for their respective policy types.
 * <p>
 * Furthermore, they each define a list of native policies available by default, source code
 * templates for new policies, and directories for where policy implementations are stored.
 *
 * <hr>
 * <p>
 * The {@link ispd.policy.managers.util util} sub-package contains two classes:
 * <ul>
 *     <li>{@link ispd.policy.managers.util.JarExtractionUtility JarExtractionUtility}</li>
 *     <li>{@link ispd.policy.managers.util.JavaCompilationUtility JavaCompilationUtility}</li>
 * </ul>
 * Both classes are used by {@link ispd.policy.managers.FilePolicyManager FilePolicyManager}'s
 * implementation.
 */
package ispd.policy.managers;