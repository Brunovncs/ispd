package ispd.policy.managers;

import ispd.arquivo.xml.ConfiguracaoISPD;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyManager;
import ispd.policy.scheduling.cloud.CloudSchedulingPolicy;

import java.io.File;
import java.util.List;

/**
 * This class is a specialization of the {@link FilePolicyManager} class for <i>cloud scheduling</i>
 * policies.
 * <p>
 * This class also defines a list of native cloud scheduling policies available by default, which
 * can be accessed via the {@link #NATIVE_POLICIES} static field.
 *
 * @see FilePolicyManager The base class that this class extends
 * @see PolicyManager The interface that this class and its base class implement
 */
public class CloudSchedulingPolicyManager extends FilePolicyManager {
    /**
     * A list of native cloud scheduling policies available to the user by default.
     * <p>
     * The list includes the following policies' names:
     * <ul>
     *     <li>{@link ispd.policy.scheduling.cloud.impl.RoundRobin RoundRobin}</li>
     * </ul>
     * <p>
     * It also contains the {@link #NO_POLICY} constant ({@value #NO_POLICY}), which represents
     * the absence of a policy, for convenience of selection menus that wish to use this list.
     */
    public static final List<String> NATIVE_POLICIES = List.of(
            PolicyManager.NO_POLICY, "RoundRobin"
    );

    /**
     * The path to the directory where cloud scheduling policies are stored on the filesystem,
     * relative to the ISPD root directory (as specified by the
     * {@link ConfiguracaoISPD#DIRETORIO_ISPD} field).
     * <p>
     * The full path is: {@code [DiretorioISPD]/policies/scheduling/cloud/}
     */
    private static final String CLOUD_DIR_PATH =
            String.join(File.separator, "policies", "scheduling", "cloud");

    /**
     * The {@link File} instance corresponding to the directory where cloud scheduling policies are
     * stored on the filesystem.
     * <p>
     * The directory is located under the iSPD root directory (as specified by the
     * {@link ConfiguracaoISPD#DIRETORIO_ISPD} field).
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/scheduling/cloud/}
     */
    private static final File CLOUD_DIRECTORY =
            new File(ConfiguracaoISPD.DIRETORIO_ISPD, CloudSchedulingPolicyManager.CLOUD_DIR_PATH);
    private static final String CLOUD_PKG = "escalonadorCloud";

    /**
     * The {@link File} instance corresponding to the directory where cloud scheduling policies are
     * stored on the filesystem.
     * <p>
     * The directory is as defined in the {@link #CLOUD_DIRECTORY} static field.
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/scheduling/cloud/}
     *
     * @return the directory where cloud scheduling policies are stored on the filesystem.
     */
    @Override
    public File getPolicyDirectory() {
        return CloudSchedulingPolicyManager.CLOUD_DIRECTORY;
    }

    /**
     * Returns the name of the package where cloud scheduling policies are stored.
     * <p>
     * The package name is {@value #CLOUD_PKG}.
     *
     * @return the name of the package where cloud scheduling policies are stored.
     */
    @Override
    protected String getPolicyPackageName() {
        return CloudSchedulingPolicyManager.CLOUD_PKG;
    }

    /**
     * Returns the source code 'raw' template for cloud scheduling policies.
     * <hr>
     * <p>
     * The template includes a class that extends the abstract
     * {@link CloudSchedulingPolicy CloudSchedulingPolicy}, overriding its abstract methods with
     * implementations that merely throw an {@link UnsupportedOperationException}.
     * <p>
     * These implementations must be substituted by the user in order to create a functional
     * scheduling policy. The overridden methods that must be properly implemented are:
     * <ul>
     *     <li>{@link CloudSchedulingPolicy#initialize() initialize}</li>
     *     <li>{@link CloudSchedulingPolicy#schedule() schedule}</li>
     *     <li>{@link CloudSchedulingPolicy#scheduleRoute(CentroServico) scheduleRoute}</li>
     *     <li>{@link CloudSchedulingPolicy#scheduleResource() scheduleResource}</li>
     *     <li>{@link CloudSchedulingPolicy#scheduleTask() scheduleTask}</li>
     * </ul>
     * <p>
     * The class name is left as {@value FilePolicyManager#POLICY_NAME_REPL}, to be replaced
     * during runtime, as specified by {@link FilePolicyManager#getPolicyTemplate(String)}.
     *
     * @return the source code template for cloud scheduling policies.
     *
     * @see FilePolicyManager#POLICY_NAME_REPL
     * @see FilePolicyManager#getPolicyTemplate(String)
     * @see CloudSchedulingPolicy
     * @see ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy
     * @see ispd.policy.Policy Policy
     */
    protected String getRawPolicyTemplate() {
        //language=JAVA
        return """
                import ispd.policy.scheduling.cloud.CloudSchedulingPolicy;
                import ispd.motor.filas.Tarefa;
                import ispd.motor.filas.servidores.CS_Processamento;
                import ispd.motor.filas.servidores.CentroServico;
                                
                import java.util.List;
                                
                public class __POLICY_NAME__ extends CloudSchedulingPolicy {
                    @Override
                    public void initialize() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                           
                    @Override
                    public void schedule() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public List<CentroServico> scheduleRoute(final CentroServico destination) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public CS_Processamento scheduleResource() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public Tarefa scheduleTask() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                }
                               """;
    }
}
