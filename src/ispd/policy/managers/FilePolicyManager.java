package ispd.policy.managers;

import ispd.policy.PolicyManager;
import ispd.policy.managers.util.JarExtractionUtility;
import ispd.policy.managers.util.JavaCompilationUtility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Abstract base class implementation of the {@link PolicyManager} interface, for concrete policy
 * managers that use file-based storage for policies.
 * <p>
 * The class provides a set of methods for reading, saving, compiling, and importing policies from
 * the filesystem. It also maintains lists of available, added, and removed policies.
 * <p>
 * Concrete policy managers should extend this class and implement the
 * {@link #getPolicyPackageName()} and {@link #getRawPolicyTemplate()}} methods to specify the
 * appropriate package name and code template for the policy type that they manage.
 * <p>
 * It is expected that each policy type will have its own specialization of this class.
 *
 * @see PolicyManager The interface that this class implements
 * @see GridSchedulingPolicyManager Specialization of this class for <i>grid scheduling</i> policies
 * @see CloudSchedulingPolicyManager Specialization of this class for <i>cloud scheduling</i>
 * policies
 * @see VmAllocationPolicyManager Specialization of this class for <i>VM allocation</i> policies
 */
public abstract class FilePolicyManager implements PolicyManager {
    /**
     * String which is replaced in source code returned by {@link #getRawPolicyTemplate()} by the
     * proper name of the policy being constructed in calls to the method
     * {@link #getPolicyTemplate(String)}.
     *
     * @see #getPolicyTemplate(String)
     * @see #getRawPolicyTemplate()
     */
    protected static final String POLICY_NAME_REPL = "__POLICY_NAME__";
    private static final String CLASS_FILE_SUFFIX = ".class";
    private static final String JAVA_FILE_SUFFIX = ".java";
    private final List<String> availablePolicies = new ArrayList<>();
    private final List<String> addedPolicies = new ArrayList<>();
    private final List<String> removedPolicies = new ArrayList<>();

    /**
     * Common constructor for new instances of subclasses.
     * <p>
     * This constructor initializes the available policies by searching for class files in the
     * policy directory and extracting policy source code from any found jar files.
     *
     * @see #initialize()
     */
    protected FilePolicyManager() {
        this.initialize();
    }

    /**
     * Initializes the available policies by searching for {@code .class} files in the policy
     * directory, if such exists, otherwise extracting policy bytecode files from the application's
     * jar file (if it is determined to be executing from one).
     * <p>
     * For determining if the application is running from a jar file, and extracting the required
     * directories from the it, this method uses the {@link JarExtractionUtility} class.
     *
     * @see #getPolicyDirectory()
     * @see #loadPoliciesFromFoundDotClassFiles()
     * @see JarExtractionUtility
     */
    private void initialize() {
        if (this.getPolicyDirectory().exists()) {
            this.loadPoliciesFromFoundDotClassFiles();
            return;
        }

        try {
            Files.createDirectory(this.getPolicyDirectory().toPath());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }

        if (JarExtractionUtility.isApplicationExecutingFromJar()) {
            try {
                new JarExtractionUtility(this.getPolicyPackageName()).extractDirsFromJar();
            } catch (final IOException e) {
                FilePolicyManager.severeLog(e);
            }
        }
    }

    /**
     * Loads the available policies' names into the {@link #availablePolicies} list, by searching
     * for {@code .class} files in the policy directory, as given by the
     * {@link #getPolicyDirectory()} method.
     */
    private void loadPoliciesFromFoundDotClassFiles() {
        final FilenameFilter f = (b, name) -> name.endsWith(FilePolicyManager.CLASS_FILE_SUFFIX);

        /*
         * {@link File#list()} returns {@code null} on I/O error (or if the given {@link File} is
         *  not a directory that exists, but that situation has already been accounted for).
         */
        final var dotClassFiles = Objects.requireNonNull(this.getPolicyDirectory().list(f));

        Arrays.stream(dotClassFiles)
                .map(FilePolicyManager::removeDotClassSuffix)
                .forEach(this.availablePolicies::add);
    }

    private static String removeDotClassSuffix(final String s) {
        return FilePolicyManager.removeSuffix(s, FilePolicyManager.CLASS_FILE_SUFFIX);
    }

    /**
     * Method intended to remove the suffix from a string. However, it merely slices the original
     * string to leave out at the end the desired number of characters. <i>It does <b>not</b> check
     * if the input {@code str} actually terminates with the given {@code suffix}.</i>
     *
     * @param str    the string to remove the suffix from.
     * @param suffix the suffix to remove.
     *
     * @return the string with the suffix removed. (More accurately, the string with the number of
     * characters in suffix removed from its end.)
     *
     * @throws IndexOutOfBoundsException if the length of {@code suffix} is greater than that of
     *                                   {@code str}.
     */
    private static String removeSuffix(final String str, final String suffix) {
        return str.substring(0, str.length() - suffix.length());
    }

    /**
     * Returns the package name of the policy type handled by this manager.
     * <p>
     * This method is used internally to extract the policy files from the application's jar file.
     * <p>
     * Concrete subclasses should implement this method to specify the package name of the policy
     * type they are responsible for.
     *
     * @return the package name of the policy type handled by this manager.
     */
    protected abstract String getPolicyPackageName();

    private static void severeLog(final Throwable e) {
        Logger.getLogger(FilePolicyManager.class.getName()).log(Level.SEVERE, null, e);
    }

    @Override
    public List<String> listAvailablePolicies() {
        return this.availablePolicies;
    }

    @Override
    public List<String> listAddedPolicies() {
        return this.addedPolicies;
    }

    @Override
    public List<String> listRemovedPolicies() {
        return this.removedPolicies;
    }

    @Override
    public String getPolicyCode(final String policyName) {
        try (final var br = new BufferedReader(new FileReader(
                this.policySourceCodeFile(policyName),
                StandardCharsets.UTF_8
        ))) {
            return br.lines().collect(Collectors.joining("\n"));
        } catch (final IOException ex) {
            FilePolicyManager.severeLog(ex);
            return null;
        }
    }

    @Override
    public void savePolicy(final String policyName, final String policyCode) {
        try (final var fw = new FileWriter(
                this.policySourceCodeFile(policyName),
                StandardCharsets.UTF_8
        )) {
            fw.write(policyCode);
        } catch (final IOException ex) {
            FilePolicyManager.severeLog(ex);
        }
    }

    @Override
    public String compilePolicy(final String policyName) {
        final var target = this.policySourceCodeFile(policyName);
        final var err = FilePolicyManager.compile(target);

        try {
            /*
              {@link Runtime#exec(String)} runs on a <i>separate</i> process, so we need to wait
               for potential compilation time.
              <br>
              However, this solution is <i>bad</i> because the compilation may easily take longer
               than {@code 1000} milliseconds (complicated target files or low-end systems
               running the application).
              <br>
              Thus, we need to look for alternatives in the future.
             */
            Thread.sleep(1000);
        } catch (final InterruptedException ex) {
            FilePolicyManager.severeLog(ex);
        }

        if (this.checkIfBytecodeFileExists(policyName)) {
            this.addPolicyToList(policyName);
        }

        return err.isEmpty() ? null : err;
    }

    /**
     * Checks if the bytecode file (with {@code .class} extension) associated with the policy of
     * given name exists.
     *
     * @param policyName name of the policy whose bytecode file must be determined if exists.
     *
     * @return {@code true} if the bytecode file for the policy of given name exists, {@code false}
     * otherwise.
     *
     * @see #policyBytecodeFile(String)
     */
    private boolean checkIfBytecodeFileExists(final String policyName) {
        return this.policyBytecodeFile(policyName).exists();
    }

    /**
     * The bytecode file that will be (or is) associated with the policy of given name, as a
     * {@link File} instance.
     *
     * @param policyName name of the policy.
     *
     * @return {@link File} instance representing the bytecode file associated with the policy of
     * given name.
     */
    private File policyBytecodeFile(final String policyName) {
        return this.policyFileWithExtension(policyName, FilePolicyManager.CLASS_FILE_SUFFIX);
    }

    /**
     * Compile the given source code file, using the {@link JavaCompilationUtility} class.
     *
     * @param target source code file to compile.
     *
     * @return {@link String} with errors during the compilation, empty in the case of a successful
     * compilation.
     */
    private static String compile(final File target) {
        return new JavaCompilationUtility(target).compile();
    }

    /**
     * Adds the policy of given name to the internal lists of {@link #availablePolicies} and
     * {@link #addedPolicies}. Does nothing the if {@link #availablePolicies} list already contains
     * a policy with the given name.
     *
     * @param policyName name of the policy to attempt adding.
     */
    private void addPolicyToList(final String policyName) {
        if (this.availablePolicies.contains(policyName)) {
            return;
        }

        this.availablePolicies.add(policyName);
        this.addedPolicies.add(policyName);
    }

    @Override
    public boolean importPolicy(final File file) {
        final var target = new File(this.getPolicyDirectory(), file.getName());
        FilePolicyManager.transferFileContents(file, target);

        final var err = FilePolicyManager.compile(target);

        if (!err.isEmpty()) {
            return false;
        }

        final var policyName = FilePolicyManager.removeSuffix(
                file.getName(), FilePolicyManager.JAVA_FILE_SUFFIX);

        if (!this.checkIfBytecodeFileExists(policyName)) {
            return false;
        }

        this.addPolicyToList(policyName);

        return true;
    }

    /**
     * This implementation operates in three steps:
     * <ol>
     *     <li>Removing the policy's <i>bytecode</i> file.</li>
     *     <li>Removing the respective item from the internal {@link #availablePolicies} list.</li>
     *     <li>Removing the policy's <i>source code</i> file.</li>
     * </ol>
     * If an error occurs at any stage, subsequent stages are not executed and the method returns
     * {@code false}. Otherwise, {@code true}.
     */
    @Override
    public boolean removePolicy(final String policyName) {
        try {
            Files.delete(this.policyBytecodeFile(policyName).toPath());
            this.removePolicyFromList(policyName);
            Files.delete(this.policySourceCodeFile(policyName).toPath());
            return true;
        } catch (final IOException e) {
            return false;
        }
    }

    @Override
    public String getPolicyTemplate(final String policyName) {
        return this.getRawPolicyTemplate().replace(
                FilePolicyManager.POLICY_NAME_REPL,
                policyName
        );
    }

    /**
     * Returns the raw template for writing a new policy's source code.
     * <hr>
     * <p>
     * The template should contain a class definition implementing the appropriate
     * {@link ispd.policy.Policy} specialization's methods. It should also contain a placeholder for
     * the policy name, namely the string {@value POLICY_NAME_REPL}.
     * <p>
     * This placeholder is replaced with the appropriate contents by the superclass when the method
     * {@link #getPolicyTemplate(String) getPolicyTemplate} is called.
     *
     * @return the raw template for writing a new policy's source code.
     */
    protected abstract String getRawPolicyTemplate();

    /**
     * Removes the policy of given name to the internal list of {@link #availablePolicies}, and adds
     * it to the list of {@link #removedPolicies}. Does nothing the if {@link #availablePolicies}
     * list <b>does not</b> contains a policy with the given name.
     *
     * @param policyName name of the policy to attempt removing.
     */
    private void removePolicyFromList(final String policyName) {
        if (!this.availablePolicies.contains(policyName)) {
            return;
        }

        this.availablePolicies.remove(policyName);
        this.removedPolicies.add(policyName);
    }

    private static void transferFileContents(final File src, final File dest) {
        if (src.getPath().equals(dest.getPath())) {
            return;
        }

        try (final var srcFs = new FileInputStream(src);
             final var destFs = new FileOutputStream(dest)) {
            srcFs.transferTo(destFs);
        } catch (final IOException ex) {
            FilePolicyManager.severeLog(ex);
        }
    }

    private File policySourceCodeFile(final String policyName) {
        return this.policyFileWithExtension(policyName, FilePolicyManager.JAVA_FILE_SUFFIX);
    }

    private File policyFileWithExtension(final String policyName, final String ext) {
        return new File(this.getPolicyDirectory(), policyName + ext);
    }
}
