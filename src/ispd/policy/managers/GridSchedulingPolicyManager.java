package ispd.policy.managers;

import ispd.arquivo.xml.ConfiguracaoISPD;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyManager;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.io.File;
import java.util.List;

/**
 * This class is a specialization of the {@link FilePolicyManager} class for <i>grid scheduling</i>
 * policies.
 * <p>
 * This class also defines a list of native grid scheduling policies available by default, which can
 * be accessed via the {@link #NATIVE_POLICIES} static field.
 *
 * @see FilePolicyManager The base class that this class extends
 * @see PolicyManager The interface that this class and its base class implement
 */
public class GridSchedulingPolicyManager extends FilePolicyManager {
    /**
     * A list of native grid scheduling policies available to the user by default.
     * <p>
     * The list includes the following policies' names:
     * <ul>
     *     <li>{@link ispd.policy.scheduling.grid.impl.RoundRobin RoundRobin}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.Workqueue Workqueue}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.WQR WQR}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.DynamicFPLTF DynamicFPLTF}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.OSEP OSEP}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.M_OSEP M_OSEP}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.HOSEP HOSEP}</li>
     *     <li>{@link ispd.policy.scheduling.grid.impl.EHOSEP EHOSEP}</li>
     * </ul>
     * <p>
     * It also contains the {@link #NO_POLICY} constant ({@value #NO_POLICY}), which represents
     * the absence of a policy, for convenience of selection menus that wish to use this list.
     */
    public static final List<String> NATIVE_POLICIES = List.of(
            PolicyManager.NO_POLICY,
            "RoundRobin", "Workqueue", "WQR", "DynamicFPLTF",
            "OSEP", "M_OSEP", "HOSEP", "EHOSEP"
    );

    /**
     * The path to the directory where grid scheduling policies are stored on the filesystem,
     * relative to the ISPD root directory (as specified by the
     * {@link ConfiguracaoISPD#DIRETORIO_ISPD} field).
     * <p>
     * The full path is: {@code [DiretorioISPD]/policies/scheduling/grid/}
     */
    private static final String GRID_DIR_PATH =
            String.join(File.separator, "policies", "scheduling", "grid");

    /**
     * The {@link File} instance corresponding to the directory where grid scheduling policies are
     * stored on the filesystem.
     * <p>
     * The directory is located under the iSPD root directory (as specified by the
     * {@link ConfiguracaoISPD#DIRETORIO_ISPD} field).
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/scheduling/grid/}
     */
    private static final File GRID_DIRECTORY =
            new File(ConfiguracaoISPD.DIRETORIO_ISPD, GridSchedulingPolicyManager.GRID_DIR_PATH);
    private static final String GRID_PKG = "escalonador";

    /**
     * The {@link File} instance corresponding to the directory where grid scheduling policies are
     * stored on the filesystem.
     * <p>
     * The directory is as defined in the {@link #GRID_DIRECTORY} static field.
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/scheduling/grid/}
     *
     * @return the directory where grid scheduling policies are stored on the filesystem.
     */
    @Override
    public File getPolicyDirectory() {
        return GridSchedulingPolicyManager.GRID_DIRECTORY;
    }

    /**
     * Returns the name of the package where grid scheduling policies are stored.
     * <p>
     * The package name is {@value #GRID_PKG}.
     *
     * @return the name of the package where grid scheduling policies are stored.
     */
    @Override
    protected String getPolicyPackageName() {
        return GridSchedulingPolicyManager.GRID_PKG;
    }

    /**
     * Returns the source code 'raw' template for grid scheduling policies.
     * <hr>
     * <p>
     * The template includes a class that extends the abstract
     * {@link GridSchedulingPolicy GridSchedulingPolicy}, overriding its abstract methods with
     * implementations that merely throw an {@link UnsupportedOperationException}.
     * <p>
     * These implementations must be substituted by the user in order to create a functional
     * scheduling policy. The overridden methods that must be properly implemented are:
     * <ul>
     *     <li>{@link GridSchedulingPolicy#initialize() initialize}</li>
     *     <li>{@link GridSchedulingPolicy#schedule() schedule}</li>
     *     <li>{@link GridSchedulingPolicy#scheduleRoute(CentroServico) scheduleRoute}</li>
     *     <li>{@link GridSchedulingPolicy#scheduleResource() scheduleResource}</li>
     *     <li>{@link GridSchedulingPolicy#scheduleTask() scheduleTask}</li>
     * </ul>
     * <p>
     * The class name is left as {@value FilePolicyManager#POLICY_NAME_REPL}, to be replaced
     * during runtime, as specified by {@link FilePolicyManager#getPolicyTemplate(String)}.
     *
     * @return the source code template for grid scheduling policies.
     *
     * @see FilePolicyManager#POLICY_NAME_REPL
     * @see FilePolicyManager#getPolicyTemplate(String)
     * @see GridSchedulingPolicy
     * @see ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy
     * @see ispd.policy.Policy Policy
     */
    @Override
    protected String getRawPolicyTemplate() {
        //language=JAVA
        return """
                import ispd.motor.filas.Tarefa;
                import ispd.motor.filas.servidores.CS_Processamento;
                import ispd.motor.filas.servidores.CentroServico;
                import ispd.policy.scheduling.grid.GridSchedulingPolicy;
                                
                import java.util.List;
                                
                public class __POLICY_NAME__ extends GridSchedulingPolicy {
                    @Override
                    public void initialize() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public void schedule() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public List<CentroServico> scheduleRoute(final CentroServico destination) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public CS_Processamento scheduleResource() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                                
                    @Override
                    public Tarefa scheduleTask() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                }
                               """;
    }
}
