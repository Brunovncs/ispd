/**
 * The {@code vmallocation} package contains:
 * <ul>
 *     <li>{@link ispd.policy.vmallocation.VirtualMachineMaster VirtualMachineMaster}: A VM
 *     allocation specialization of {@link ispd.policy.PolicyMaster PolicyMaster}.</li>
 *     <li>{@link ispd.policy.vmallocation.VmAllocationPolicy VmAllocationPolicy}: A VM
 *     allocation specialization of {@link ispd.policy.Policy Policy}.</li>
 *     <li>The subpackage {@link ispd.policy.vmallocation.impl impl}, which holds implementations
 *     for concrete VM allocation policies.</li>
 * </ul>
 */
package ispd.policy.vmallocation;