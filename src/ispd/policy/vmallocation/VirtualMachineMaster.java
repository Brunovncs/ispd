package ispd.policy.vmallocation;

import ispd.motor.FutureEvent;
import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.PolicyMaster;

/**
 * The {@code VirtualMachineMaster} interface represents a class that can execute virtual machine
 * allocation operations by communicating with the currently unfolding
 * {@link ispd.motor.Simulation Simulation}. It is only responsible for managing VM allocations.
 * <p>
 * It extends the {@link PolicyMaster} interface and adds two methods specific to allocation
 * policies: {@link #executeAllocation}, which emulates the execution of a VM allocation, and
 * {@link #sendVirtualMachine}, which sends a virtual machine to its appropriate master.
 * <p>
 * This class has no further specializations. A concrete implementation of this interface is
 * {@link ispd.motor.filas.servidores.implementacao.CS_VMM CS_VMM}.
 * <p>
 * This type of master is associated with the {@link VmAllocationPolicy} policy type.
 *
 * @see PolicyMaster The base interface for classes that can call methods on scheduling or
 * allocation policies
 * @see VmAllocationPolicy Policy type that uses this master type.
 * @see ispd.motor.filas.servidores.implementacao.CS_VMM Concrete implementation of the interface
 */
public interface VirtualMachineMaster extends PolicyMaster {
    /**
     * Add an {@link ispd.motor.FutureEvent#ALOCAR_VMS ALOCAR_VMS}-type event associated iwth this
     * master to the {@link ispd.motor.Simulation Simulation}'s queue.
     *
     * @see ispd.motor.FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    void executeAllocation();

    /**
     * Send the specified {@link CS_VirtualMac virtual machine} to its appropriate
     * {@link ispd.motor.filas.servidores.implementacao.CS_VMM CS_VMM} master.
     * <p>
     * To do so, creates a new {@link ispd.motor.filas.TarefaVM vm task} to represent the allocation
     * effort, destined for the given {@link CS_VirtualMac VM}. Then, creates a new
     * {@link FutureEvent} of type {@link FutureEvent#SAIDA} (of this created task leaving this
     * resource).
     *
     * @param virtualMachine the {@link CS_VirtualMac} to be allocated.
     *
     * @see ispd.motor.filas.TarefaVM
     * @see FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    void sendVirtualMachine(CS_VirtualMac virtualMachine);
}
