package ispd.policy.vmallocation.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.motor.filas.servidores.implementacao.CS_MaquinaCloud;
import ispd.motor.filas.servidores.implementacao.CS_VMM;
import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.vmallocation.VmAllocationPolicy;
import ispd.policy.vmallocation.impl.util.ComparaRequisitos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ConcretePolicy
public class FirstFitDecreasing extends VmAllocationPolicy {
    private final ComparaRequisitos comparaReq;
    private boolean fit = false;
    private int maqIndex = 0;
    private List<CS_VirtualMac> VMsOrdenadas = null;

    public FirstFitDecreasing() {
        this.virtualMachines = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.rejectedVMs = new ArrayList<>();
        this.comparaReq = new ComparaRequisitos();
    }

    @Override
    public void initialize() {
        this.fit = true;
        this.maqIndex = 0;
        this.VMsOrdenadas = new ArrayList<>(this.virtualMachines);
        this.VMsOrdenadas.sort(this.comparaReq);
        Collections.reverse(this.VMsOrdenadas);
        if (!this.slaves.isEmpty() && !this.virtualMachines.isEmpty()) {
            this.schedule();
        }
    }

    @Override
    public void schedule() {
        while (!(this.virtualMachines.isEmpty())) {
            int num_escravos = this.slaves.size();

            final CS_VirtualMac auxVM = this.scheduleVirtualMachine();

            while (num_escravos >= 0) {
                if (num_escravos > 0) {//caso existam máquinas livres
                    final var auxMaq = this.scheduleResource();
                    //escalona
                    // o recurso
                    if (auxMaq instanceof CS_VMM) {

                        auxVM.setCaminho(this.scheduleRoute(auxMaq));
                        //salvando uma lista de VMMs intermediarios no
                        // caminho da vm e seus respectivos caminhos
                        this.policyMaster.sendVirtualMachine(auxVM);
                        break;
                    } else {
                        final CS_MaquinaCloud maq = (CS_MaquinaCloud) auxMaq;
                        final double memoriaMaq = maq.getMemoriaDisponivel();
                        final double memoriaNecessaria =
                                auxVM.getMemoriaDisponivel();
                        final double discoMaq = maq.getDiscoDisponivel();
                        final double discoNecessario =
                                auxVM.getDiscoDisponivel();
                        final int maqProc = maq.getProcessadoresDisponiveis();
                        final int procVM = auxVM.getProcessadoresDisponiveis();

                        if ((memoriaNecessaria <= memoriaMaq && discoNecessario <= discoMaq && procVM <= maqProc)) {
                            maq.setMemoriaDisponivel(memoriaMaq - memoriaNecessaria);
                            maq.setDiscoDisponivel(discoMaq - discoNecessario);
                            maq.setProcessadoresDisponiveis(maqProc - procVM);
                            auxVM.setMaquinaHospedeira((CS_MaquinaCloud) auxMaq);
                            auxVM.setCaminho(this.scheduleRoute(auxMaq));
                            this.policyMaster.sendVirtualMachine(auxVM);
                            break;
                        } else {
                            num_escravos--;
                        }
                    }
                } else {
                    auxVM.setStatus(CS_VirtualMac.REJEITADA);
                    this.rejectedVMs.add(auxVM);
                    num_escravos--;
                }
            }
        }
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public CS_Processamento scheduleResource() {
        if (this.fit) {
            return this.slaves.get(0);
        } else {
            return this.slaves.get(this.maqIndex);
        }
    }

    @Override
    public CS_VirtualMac scheduleVirtualMachine() {
        return this.VMsOrdenadas.remove(0);
    }
}
