package ispd.policy.vmallocation.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.motor.filas.servidores.implementacao.CS_MaquinaCloud;
import ispd.motor.filas.servidores.implementacao.CS_VMM;
import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.vmallocation.VmAllocationPolicy;

import java.util.ArrayList;
import java.util.List;

@ConcretePolicy
public class FirstFit extends VmAllocationPolicy {
    private boolean fit;
    private int maqIndex;

    public FirstFit() {
        this.virtualMachines = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.rejectedVMs = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.fit = true;
        this.maqIndex = 0;

        if (!this.slaves.isEmpty() && !this.virtualMachines.isEmpty()) {
            this.schedule();
        }
    }

    @Override
    public void schedule() {
        while (!(this.virtualMachines.isEmpty())) {
            var slaveCount = this.slaves.size();
            final var auxVM = this.scheduleVirtualMachine();

            do {
                if (slaveCount == 0) {
                    auxVM.setStatus(CS_VirtualMac.REJEITADA);
                    this.rejectedVMs.add(auxVM);
                    this.maqIndex = 0;
                    slaveCount--;
                    continue;
                }

                final var auxMaq = this.scheduleResource();
                this.maqIndex++;

                if (auxMaq instanceof CS_VMM) {
                    auxVM.setCaminho(this.scheduleRoute(auxMaq));
                    this.policyMaster.sendVirtualMachine(auxVM);
                    break;
                } else {
                    final var maq = (CS_MaquinaCloud) auxMaq;
                    if (FirstFit.canMachineFitVm(maq, auxVM)) {
                        FirstFit.makeMachineHostVm(maq, auxVM);
                        auxVM.setCaminho(this.scheduleRoute(auxMaq));

                        this.policyMaster.sendVirtualMachine(auxVM);
                        this.maqIndex = 0;
                        this.fit = true;

                        break;
                    } else {
                        slaveCount--;
                        this.fit = false;
                    }
                }
            } while (slaveCount >= 0);
        }
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public CS_Processamento scheduleResource() {
        return this.slaves.get(this.fit ? 0 : this.maqIndex);
    }

    @Override
    public CS_VirtualMac scheduleVirtualMachine() {
        return this.virtualMachines.remove(0);
    }

    private static boolean canMachineFitVm(
            final CS_MaquinaCloud machine, final CS_VirtualMac vm) {
        return vm.getMemoriaDisponivel() <= machine.getMemoriaDisponivel()
               && vm.getDiscoDisponivel() <= machine.getDiscoDisponivel()
               && vm.getProcessadoresDisponiveis() <= machine.getProcessadoresDisponiveis();
    }

    private static void makeMachineHostVm(
            final CS_MaquinaCloud machine, final CS_VirtualMac vm) {
        machine.setMemoriaDisponivel(machine.getMemoriaDisponivel() - vm.getMemoriaDisponivel());
        machine.setDiscoDisponivel(machine.getDiscoDisponivel() - vm.getDiscoDisponivel());
        machine.setProcessadoresDisponiveis(machine.getProcessadoresDisponiveis() - vm.getProcessadoresDisponiveis());
        vm.setMaquinaHospedeira(machine);
    }
}
