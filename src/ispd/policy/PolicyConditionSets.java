package ispd.policy;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * A utility class that provides sets representing unions of different
 * {@link PolicyCondition policy conditions}.
 * <p>
 * All sets provided by this class are in <b>static</b> fields and are <i>wrapped with
 * {@link Collections#unmodifiableCollection(Collection) unmodifiable collections}.</i> Attempting
 * to modify any fields of this class will result in an {@link UnsupportedOperationException} being
 * thrown.
 *
 * @see #ALL
 * @see #WHEN_RECEIVES_RESULT
 * @see #WHILE_MUST_DISTRIBUTE
 */
public class PolicyConditionSets {
    /**
     * A set with all possible {@link PolicyCondition} values.
     */
    public static final Set<PolicyCondition> ALL =
            Collections.unmodifiableSet(EnumSet.allOf(PolicyCondition.class));
    /**
     * A set containing only the {@link PolicyCondition#WHEN_RECEIVES_RESULT} directive.
     */
    @SuppressWarnings("unused")
    public static final Set<PolicyCondition> WHEN_RECEIVES_RESULT =
            Collections.unmodifiableSet(EnumSet.of(PolicyCondition.WHEN_RECEIVES_RESULT));
    /**
     * A set containing only the {@link PolicyCondition#WHILE_MUST_DISTRIBUTE} directive.
     */
    public static final Set<PolicyCondition> WHILE_MUST_DISTRIBUTE =
            Collections.unmodifiableSet(EnumSet.of(PolicyCondition.WHILE_MUST_DISTRIBUTE));

    /**
     * Private constructor to avoid instancing this utility class.
     */
    private PolicyConditionSets() {
    }
}
