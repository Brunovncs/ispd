package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.List;

@ConcretePolicy
public class WQR extends GridSchedulingPolicy {
    private Tarefa ultimaTarefaConcluida = null;
    private List<Tarefa> tarefaEnviada = null;
    private int servidoresOcupados = 0;
    private int cont = 0;

    public WQR() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.tarefaEnviada = new ArrayList<>(this.slaves.size());
        for (int i = 0; i < this.slaves.size(); i++) {
            this.tarefaEnviada.add(null);
        }
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public void schedule() {
        final CS_Processamento rec = this.scheduleResource();
        boolean sair = false;
        if (rec != null) {
            final Tarefa trf = this.scheduleTask();
            if (trf != null) {
                if (this.tarefaEnviada.get(this.slaves.indexOf(rec)) != null) {
                    this.policyMaster.sendMessage(this.tarefaEnviada.get(this.slaves.indexOf(rec)),
                            rec, Mensagens.CANCELAR);
                } else {
                    this.servidoresOcupados++;
                }
                this.tarefaEnviada.set(this.slaves.indexOf(rec), trf);
                this.ultimaTarefaConcluida = null;
                trf.setLocalProcessamento(rec);
                trf.setCaminho(this.scheduleRoute(rec));
                this.policyMaster.sendTask(trf);
            } else if (this.tasks.isEmpty()) {
                sair = true;
            }
        }
        if (this.servidoresOcupados > 0 && this.servidoresOcupados < this.slaves.size() && this.tasks.isEmpty() && !sair) {
            for (final Tarefa tar : this.tarefaEnviada) {
                if (tar != null && tar.getOrigem().equals(this.policyMaster)) {
                    this.policyMaster.executeScheduling();
                    break;
                }
            }
        }
    }

    @Override
    public Tarefa scheduleTask() {
        if (!this.tasks.isEmpty()) {
            return this.tasks.remove(0);
        }
        if (this.cont >= this.tarefaEnviada.size()) {
            this.cont = 0;
        }
        if (this.servidoresOcupados >= this.slaves.size()) {
            return null;
        }
        for (int i = this.cont; i < this.tarefaEnviada.size(); i++) {
            if (this.tarefaEnviada.get(i) != null) {
                this.cont = i;
                if (!this.tarefaEnviada.get(i).getOrigem().equals(this.policyMaster)) {
                    this.cont++;
                    return null;
                }
                return this.policyMaster.cloneTask(this.tarefaEnviada.get(i));
            }
        }
        return null;
    }

    @Override
    public void addCompletedTask(final Tarefa completedTask) {
        super.addCompletedTask(completedTask);
        final int index = this.tarefaEnviada.indexOf(completedTask);
        if (index != -1) {
            this.servidoresOcupados--;
            this.tarefaEnviada.set(index, null);
        }
        for (int i = 0; i < this.tarefaEnviada.size(); i++) {
            if (this.tarefaEnviada.get(i) != null && this.tarefaEnviada.get(i).isCopyOf(completedTask)) {
                this.policyMaster.sendMessage(this.tarefaEnviada.get(i),
                        this.slaves.get(i), Mensagens.CANCELAR);
                this.servidoresOcupados--;
                this.tarefaEnviada.set(i, null);
            }
        }
        this.ultimaTarefaConcluida = completedTask;
        if ((this.servidoresOcupados > 0 && this.servidoresOcupados < this.slaves.size()) || !this.tasks.isEmpty()) {
            this.policyMaster.executeScheduling();
        }
    }

    @Override
    public CS_Processamento scheduleResource() {
        final int index =
                this.tarefaEnviada.indexOf(this.ultimaTarefaConcluida);
        if (this.ultimaTarefaConcluida != null && index != -1) {
            return this.slaves.get(index);
        } else {
            for (int i = 0; i < this.tarefaEnviada.size(); i++) {
                if (this.tarefaEnviada.get(i) == null) {
                    return this.slaves.get(i);
                }
            }
        }
        for (int i = 0; i < this.tarefaEnviada.size(); i++) {
            if (this.tarefaEnviada.get(i) != null && this.tarefaEnviada.get(i).isCopy()) {
                return this.slaves.get(i);
            }
        }
        return null;
    }
}
