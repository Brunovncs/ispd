package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Client;
import ispd.motor.filas.Mensagem;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyConditionSets;
import ispd.policy.scheduling.grid.GridMaster;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ConcretePolicy
public class EHOSEP extends GridSchedulingPolicy {
    private final List<StatusUser> status = new ArrayList<>();
    private final List<ControleEscravos> controleEscravos = new ArrayList<>();
    private final List<Tarefa> esperaTarefas = new ArrayList<>();
    private final List<ControlePreempcao> controlePreempcao = new ArrayList<>();

    public EHOSEP() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.slavesTaskQueues = new ArrayList<>();
    }

    @Override
    public void initialize() {
        //Escalonamento quando chegam tarefas e quando tarefas são concluídas
        this.policyMaster.setSchedulingConditions(PolicyConditionSets.ALL);

        //Calcular poder computacional total do sistema, e consumo total do mesmo.
        double poderTotal = 0.0;
        Double consumoTotal = 0.0;
        for (final CS_Processamento csProcessamento : this.slaves) {
            poderTotal += csProcessamento.getPoderComputacional();
            consumoTotal += csProcessamento.getConsumoEnergia();
        }

        //Objetos de controle de uso e cota para cada um dos usuários
        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            //Calcular o poder computacional da porção de cada usuário
            double poderComp = 0.0;
            Double consumoPorcao = 0.0;
            for (final CS_Processamento slave : this.slaves) {
                //Se o nó corrente não é mestre e pertence ao usuário corrente
                if (!(slave instanceof GridMaster) && slave.getProprietario().equals(this.userMetrics.getUsuarios().get(i))) {
                    //Calcular o poder total da porcao do usuário corrente
                    poderComp += slave.getPoderComputacional();
                    //Calcular o consumo total da porcao do usuário correntes
                    consumoPorcao += slave.getConsumoEnergia();
                }
            }
            //Adiciona dados do usuário corrente à lista 
            this.status.add(new StatusUser(this.userMetrics.getUsuarios().get(i), i, poderComp));
            //Inserir consumo da porção nos dados do usuário
            this.status.get(i).setPowerShare(consumoPorcao);
            //Calcular a relação entre a eficiência da porção e a eficiência do sistema
            this.status.get(i).calculaRelacaoEficienciaEficienciaSisPor(poderTotal, consumoTotal);
            //Calcular o consumo máximo de energia de cada usuario
            final Double limite =
                    this.status.get(i).getPowerShare() * (this.userMetrics.getLimites().get(this.status.get(i).getNome()) / 100);
            this.status.get(i).setLimite(limite);
        }

        //Controle dos nós, com cópias das filas de cada um e da tarefa que executa em cada um
        for (int i = 0; i < this.slaves.size(); i++) {
            this.controleEscravos.add(new ControleEscravos(this.slaves.get(i).getId(),
                    new ArrayList<>()));
        }
    }

    @Override
    public void schedule() {

        int indexTarefa;
        int indexEscravo;
        StatusUser cliente;

        //Ordenar os usuários em ordem crescente de Poder Remanescente
        Collections.sort(this.status);


        for (final StatusUser statusUser : this.status) {
            cliente = statusUser;

            //Buscar tarefa para execucao
            indexTarefa = this.buscarTarefa(cliente);
            if (indexTarefa != -1) {

                //Buscar máquina para executar a tarefa definida
                indexEscravo = this.buscarRecurso(cliente, this.tasks.get(indexTarefa));
                if (indexEscravo != -1) {

                    //Se não é caso de preempção, a tarefa é configurada e enviada
                    if ("Livre".equals(this.controleEscravos.get(indexEscravo).getStatus())) {

                        final Tarefa tar = this.tasks.remove(indexTarefa);
                        tar.setLocalProcessamento(this.slaves.get(indexEscravo));
                        tar.setCaminho(this.scheduleRoute(this.slaves.get(indexEscravo)));
                        this.policyMaster.sendTask(tar);

                        //Atualização dos dados sobre o usuário
                        cliente.rmDemanda();
                        cliente.addServedNum();
                        cliente.addServedPerf(this.slaves.get(indexEscravo).getPoderComputacional());
                        cliente.addServedPower(this.slaves.get(indexEscravo).getConsumoEnergia());

                        //Controle das máquinas
                        this.controleEscravos.get(indexEscravo).setBloqueado();
                        return;
                    }

                    //Se é caso de preempção, a tarefa configurada e colocada em espera
                    if ("Ocupado".equals(this.controleEscravos.get(indexEscravo).getStatus())) {

                        final Tarefa tar = this.tasks.remove(indexTarefa);
                        tar.setLocalProcessamento(this.slaves.get(indexEscravo));
                        tar.setCaminho(this.scheduleRoute(this.slaves.get(indexEscravo)));

                        //Controle de preempção para enviar a nova tarefa no momento certo
                        final String userPreemp =
                                this.controleEscravos.get(indexEscravo).GetProcessador().get(0).getProprietario();
                        final int idTarefaPreemp =
                                this.controleEscravos.get(indexEscravo).GetProcessador().get(0).getIdentificador();
                        this.controlePreempcao.add(new ControlePreempcao(userPreemp,
                                idTarefaPreemp, tar.getProprietario(), tar.getIdentificador()));
                        this.esperaTarefas.add(tar);

                        //Solicitação de retorno da tarefa em execução e atualização da demanda
                        // do usuário
                        this.policyMaster.sendMessage(this.controleEscravos.get(indexEscravo).GetProcessador().get(0), this.slaves.get(indexEscravo), Mensagens.DEVOLVER_COM_PREEMPCAO);
                        this.controleEscravos.get(indexEscravo).setBloqueado();
                        cliente.rmDemanda();
                        return;
                    }
                }
            }
        }
    }

    private int buscarTarefa(final StatusUser usuario) {

        //Indice da tarefa na lista de tarefas
        int trf = -1;
        //Se o usuario tem demanda nao atendida e seu consumo nao chegou ao limite
        if (usuario.getDemanda() > 0 && usuario.getServedPower() < usuario.getLimite()) {
            //Procura pela menor tarefa nao atendida do usuario.
            for (int j = 0; j < this.tasks.size(); j++) {
                if (this.tasks.get(j).getProprietario().equals(usuario.getNome())) {
                    if (trf == -1) {
                        trf = j;
                    } else if (this.tasks.get(j).getTamProcessamento() < this.tasks.get(trf).getTamProcessamento()) {//Escolher a tarefa de menor tamanho do usuario
                        trf = j;
                    }
                }
            }
        }
        return trf;
    }

    private int buscarRecurso(final StatusUser cliente, final Client TarAloc) {

        /*++++++++++++++++++Buscando recurso livres++++++++++++++++++*/

        //Índice da máquina escolhida, na lista de máquinas
        int indexSelec = -1;
        //Consumo da máquina escolhida e da máquina comparada com a escolhida previamente em
        // passagem anterior do laço
        double consumoSelec = 0.0;
        double consumoMaqTestada;

        for (int i = 0; i < this.slaves.size(); i++) {

            //Verificar o limite de consumo e garantir que o escravo está de fato livre e que não
            // há nenhuma tarefa em trânsito para o escravo. É escolhido o recurso que consumir
            // menos energia pra executar a tarefa alocada.
            if ("Livre".equals(this.controleEscravos.get(i).getStatus()) && (this.slaves.get(i).getConsumoEnergia() + cliente.getServedPower()) <= cliente.getLimite()) {

                if (indexSelec == -1) {

                    indexSelec = i;
                    //Tempo para processar
                    consumoSelec =
                            TarAloc.getTamProcessamento() / this.slaves.get(i).getPoderComputacional();
                    //Consumo em Joule para processar
                    consumoSelec = consumoSelec * this.slaves.get(i).getConsumoEnergia();

                } else {
                    //Tempo para processar
                    consumoMaqTestada =
                            TarAloc.getTamProcessamento() / this.slaves.get(i).getPoderComputacional();
                    //Consumo em Joule para processar
                    consumoMaqTestada = consumoMaqTestada * this.slaves.get(i).getConsumoEnergia();

                    if (consumoSelec > consumoMaqTestada) {

                        indexSelec = i;
                        consumoSelec = consumoMaqTestada;
                    } else if (consumoSelec == consumoMaqTestada) {

                        if (this.slaves.get(i).getPoderComputacional() > this.slaves.get(indexSelec).getPoderComputacional()) {

                            indexSelec = i;
                            consumoSelec = consumoMaqTestada;
                        }
                    }
                }
            }
        }

        if (indexSelec != -1) {
            return indexSelec;
        }

        /*+++++++++++++++++Busca por usuário para preempção+++++++++++++++++*/

        //Se o usuário com maior valor de DifPoder não tem excesso de poder computacional, não há
        // usuário que possa sofrer preempção. Além disso, não ocorrerá preempção para atender
        // usuário que tem excesso.
        if (this.status.get(this.status.size() - 1).getServedPerf() <= this.status.get(this.status.size() - 1).getPerfShare() || cliente.getServedPerf() >= cliente.getPerfShare()) {
            return -1;
        }

        //Métricas e índice do usuário que possivelmente perderá recurso
        double consumoPonderadoCorrente;
        double consumoPonderadoSelec = 0.0;
        int indexUserPreemp = -1;

        //Começando pelo usuário de maior excesso
        for (int i = this.status.size() - 1; i >= 0; i--) {
            //Apenas usuários que tem excesso de poder computacional podem sofrer preempção
            if (this.status.get(i).getServedPerf() > this.status.get(i).getPerfShare()) {
                //Se ainda não foi escolhido
                if (indexUserPreemp == -1) {
                    indexUserPreemp = i;
                    //Sofre preempção o usuário com maior métrica calculada
                    consumoPonderadoSelec =
                            (this.status.get(i).getServedPower()) * this.status.get(i).getRelacaoEficienciSisPor();
                } else {

                    consumoPonderadoCorrente =
                            (this.status.get(i).getServedPower()) * this.status.get(i).getRelacaoEficienciSisPor();
                    if (consumoPonderadoCorrente > consumoPonderadoSelec) {

                        indexUserPreemp = i;
                        consumoPonderadoSelec = consumoPonderadoCorrente;
                    } else if (consumoPonderadoCorrente == consumoPonderadoSelec) {

                        if ((this.status.get(i).getServedPerf() - this.status.get(i).getPerfShare()) > (this.status.get(indexUserPreemp).getServedPerf() - this.status.get(indexUserPreemp).getPerfShare())) {
                            indexUserPreemp = i;
                            consumoPonderadoSelec = consumoPonderadoCorrente;
                        }
                    }
                }
            }
        }

        if (indexUserPreemp == -1) {
            return -1;
        }

        //Buscar recurso para preempção
        double desperdicioTestado;
        double desperdicioSelec = 0.0;

        for (int j = 0; j < this.slaves.size(); j++) {
            //Procurar recurso ocupado com tarefa do usuário que perderá máquina
            if ("Ocupado".equals(this.controleEscravos.get(j).getStatus()) && (this.slaves.get(j).getConsumoEnergia() + cliente.getServedPower()) <= cliente.getLimite()) {

                final Tarefa tarPreemp = this.controleEscravos.get(j).GetProcessador().get(0);
                if (tarPreemp.getProprietario().equals(this.status.get(indexUserPreemp).getNome())) {

                    if (indexSelec == -1) {
                        //Se há checkpointing de tarefas
                        if (tarPreemp.getCheckPoint() > 0.0) {
                            //((tempo atual - tempo em que a execução da tarefa começou no
                            // recurso)*poder computacional)%bloco de checkpointing
                            desperdicioSelec =
                                    ((this.policyMaster.getSimulation().getTime(this) - tarPreemp.getTempoInicial().get(tarPreemp.getTempoInicial().size() - 1)) * this.slaves.get(j).getPoderComputacional()) % tarPreemp.getCheckPoint();
                        } else {
                            //Se não há chekcpointin de tarefas, o desperdício é o tempo total
                            // executado para a tarefa na máquina corrente no laço
                            desperdicioSelec =
                                    (this.policyMaster.getSimulation().getTime(this) - tarPreemp.getTempoInicial().get(tarPreemp.getTempoInicial().size() - 1)) * this.slaves.get(j).getPoderComputacional();
                        }
                        indexSelec = j;
                    } else {

                        if (tarPreemp.getCheckPoint() > 0.0) {

                            desperdicioTestado =
                                    ((this.policyMaster.getSimulation().getTime(this) - tarPreemp.getTempoInicial().get(tarPreemp.getTempoInicial().size() - 1)) * this.slaves.get(j).getPoderComputacional()) % tarPreemp.getCheckPoint();
                        } else {
                            desperdicioTestado =
                                    (this.policyMaster.getSimulation().getTime(this) - tarPreemp.getTempoInicial().get(tarPreemp.getTempoInicial().size() - 1)) * this.slaves.get(j).getPoderComputacional();
                        }
                        //É escolhida a máquina de menor desperdício
                        if (desperdicioTestado < desperdicioSelec) {

                            desperdicioSelec = desperdicioTestado;
                            indexSelec = j;
                        }
                        //Se o desperdício é igual, é escolhida a máquina com menor poder
                        // computacional
                        else if (desperdicioTestado == desperdicioSelec && this.slaves.get(j).getPoderComputacional() < this.slaves.get(indexSelec).getPoderComputacional()) {
                            indexSelec = j;
                        }
                    }
                }
            }
        }

        if (indexSelec != -1) {
            if ((this.status.get(indexUserPreemp).getServedPerf() - this.slaves.get(indexSelec).getPoderComputacional()) < this.status.get(indexUserPreemp).getPerfShare()) {
                if (this.status.get(indexUserPreemp).getLimite() <= cliente.getLimite()) {
                    indexSelec = -1;
                }
            }
        }

        return indexSelec;
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>(this.slaveRoutes.get(index));
    }

    //Metodo necessario para implementar interface. Não é usado.
    @Override
    public CS_Processamento scheduleResource() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    //Definir o intervalo de tempo, em segundos, em que as máquinas enviarão dados de atualização
    // para o escalonador
    @Override
    public Double getUpdateInterval() {
        return 15.0;
    }

    //Metodo necessario para implementar interface. Não é usado.
    @Override
    public Tarefa scheduleTask() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateResult(final Mensagem message) {
        //Localizar máquina que enviou estado atualizado
        final int index = this.slaves.indexOf(message.getOrigem());

        //Atualizar listas de espera e processamento da máquina
        this.controleEscravos.get(index).setProcessador(message.getProcessadorEscravo());
        this.controleEscravos.get(index).setFila(message.getFilaEscravo());

        //Tanto alocação para recurso livre como a preempção levam dois ciclos de atualização
        // para que a máquina possa ser considerada para esacalonamento novamente

        //Primeiro ciclo
        if ("Bloqueado".equals(this.controleEscravos.get(index).getStatus())) {
            this.controleEscravos.get(index).setIncerto();
            //Segundo ciclo
        } else if ("Incerto".equals(this.controleEscravos.get(index).getStatus())) {
            //Se não está executando nada
            if (this.controleEscravos.get(index).GetProcessador().isEmpty()) {

                this.controleEscravos.get(index).setLivre();
                //Se está executando uma tarefa
            } else if (this.controleEscravos.get(index).GetProcessador().size() == 1) {

                this.controleEscravos.get(index).setOcupado();
                //Se há mais de uma tarefa e a máquina tem mais de um núcleo
            }
        }
    }

    @Override
    //Receber nova tarefa submetida ou tarefa que sofreu preemoção
    public void submitTask(final Tarefa newTask) {

        //Método herdado, obrigatório executar para obter métricas ao final da slimuação
        super.submitTask(newTask);

        //Atualização da demanda do usuário proprietário da tarefa
        for (final StatusUser statusUser : this.status) {
            if (statusUser.getNome().equals(newTask.getProprietario())) {
                statusUser.addDemanda();
            }
        }

        //Em caso de preempção
        if (newTask.getLocalProcessamento() != null) {

            //Localizar informações de estado de máquina que executou a tarefa (se houver)
            final CS_Processamento maq = (CS_Processamento) newTask.getLocalProcessamento();

            //Localizar informações armazenadas sobre a preempção em particular

            int indexControlePreemp = -1;
            int indexStatusUserAlloc = -1;
            int indexStatusUserPreemp = -1;

            for (int j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == newTask.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(newTask.getProprietario())) {
                    indexControlePreemp = j;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc())) {
                    indexStatusUserAlloc = k;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioPreemp())) {
                    indexStatusUserPreemp = k;
                }
            }

            //Localizar tarefa em espera deseignada para executar
            for (int i = 0; i < this.esperaTarefas.size(); i++) {
                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(indexControlePreemp).getAllocID()) {
                    //Enviar tarefa para execução
                    this.policyMaster.sendTask(this.esperaTarefas.remove(i));

                    //Atualizar informações de estado do usuário cuja tarefa será executada
                    this.status.get(indexStatusUserAlloc).addServedNum();
                    this.status.get(indexStatusUserAlloc).addServedPerf(maq.getPoderComputacional());
                    this.status.get(indexStatusUserAlloc).addServedPower(maq.getConsumoEnergia());

                    //Atualizar informações de estado do usuáro cuja tarefa foi interrompida
                    this.status.get(indexStatusUserPreemp).rmServedNum();
                    this.status.get(indexStatusUserPreemp).rmServedPerf(maq.getPoderComputacional());
                    this.status.get(indexStatusUserPreemp).rmServedPower(maq.getConsumoEnergia());

                    //Com a preempção feita, os dados necessários para ela são eliminados
                    this.controlePreempcao.remove(indexControlePreemp);
                    //Encerrar laço
                    break;
                }
            }
        }
    }

    @Override
    public void addCompletedTask(final Tarefa completedTask) {
        //Método herdado, obrigatório executar para obter métricas ao final da simulação
        super.addCompletedTask(completedTask);

        //Localizar informações sobre máquina que executou a tarefa e usuário proprietário da tarefa
        final CS_Processamento maq = (CS_Processamento) completedTask.getLocalProcessamento();
        final int maqIndex = this.slaves.indexOf(maq);

        if ("Ocupado".equals(this.controleEscravos.get(maqIndex).getStatus())) {

            int statusIndex = -1;

            for (int i = 0; i < this.status.size(); i++) {
                if (this.status.get(i).getNome().equals(completedTask.getProprietario())) {
                    statusIndex = i;
                }
            }

            //Atualização das informações de estado do proprietario da tarefa terminada.
            this.status.get(statusIndex).rmServedNum();
            this.status.get(statusIndex).rmServedPerf(maq.getPoderComputacional());
            this.status.get(statusIndex).rmServedPower(maq.getConsumoEnergia());

            this.controleEscravos.get(maqIndex).setLivre();
        } else if ("Bloqueado".equals(this.controleEscravos.get(maqIndex).getStatus())) {

            int indexControlePreemp = -1;
            int indexStatusUserAlloc = -1;
            int indexStatusUserPreemp = -1;

            for (int j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == completedTask.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(completedTask.getProprietario())) {
                    indexControlePreemp = j;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc())) {
                    indexStatusUserAlloc = k;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioPreemp())) {
                    indexStatusUserPreemp = k;
                    break;
                }
            }

            //Localizar tarefa em espera designada para executar
            for (int i = 0; i < this.esperaTarefas.size(); i++) {
                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(indexControlePreemp).getAllocID()) {

                    //Enviar tarefa para execução
                    this.policyMaster.sendTask(this.esperaTarefas.remove(i));

                    //Atualizar informações de estado do usuário cuja tarefa será executada
                    this.status.get(indexStatusUserAlloc).addServedNum();
                    this.status.get(indexStatusUserAlloc).addServedPerf(maq.getPoderComputacional());
                    this.status.get(indexStatusUserAlloc).addServedPower(maq.getConsumoEnergia());

                    //Atualizar informações de estado do usuário cuja tarefa teve a execução
                    // interrompida
                    this.status.get(indexStatusUserPreemp).rmServedNum();
                    this.status.get(indexStatusUserPreemp).rmServedPerf(maq.getPoderComputacional());
                    this.status.get(indexStatusUserPreemp).rmServedPower(maq.getConsumoEnergia());

                    //Com a preempção feita, os dados necessários para ela são eliminados
                    this.controlePreempcao.remove(indexControlePreemp);
                    //Encerrar laço
                    break;
                }
            }
        }
    }

    //Classe para dados de estado dos usuários
    private static class StatusUser implements Comparable<StatusUser> {
        private final String user;//Nome do usuario
        private final double perfShare;//Desempenho total das máquinas do usuário
        private int demanda;//Número de tarefas na fila
        private double powerShare;//Consumo de energia total das máquinas do usuário
        private double servedPerf;//Desempenho total que atende ao usuário
        private double servedPower;//Consumo de energia total que atende ao usuario
        private double limiteConsumo;//Limite de consumo definido pelo usuario
        private double relacaoEficienciaSistemaPorcao = 0.0;//Nova métrica para decisão de preempção

        private StatusUser(final String user, final int indexUser, final double perfShare) {
            this.user = user;
            this.demanda = 0;
            this.perfShare = perfShare;
            this.powerShare = 0.0;
            this.servedPerf = 0.0;
            this.servedPower = 0.0;
            this.limiteConsumo = 0.0;
        }

        private void calculaRelacaoEficienciaEficienciaSisPor(final Double poderSis,
                                                              final Double consumoSis) {
            this.relacaoEficienciaSistemaPorcao =
                    ((poderSis / consumoSis) / (this.perfShare / this.powerShare));
        }

        private void addDemanda() {
            this.demanda++;
        }

        private void rmDemanda() {
            this.demanda--;
        }

        private void addServedNum() {
        }

        private void rmServedNum() {
        }

        private void addServedPerf(final Double perf) {
            this.servedPerf += perf;
        }

        private void rmServedPerf(final Double perf) {
            this.servedPerf -= perf;
        }

        private void addServedPower(final Double power) {
            this.servedPower += power;
        }

        private void rmServedPower(final Double power) {
            this.servedPower -= power;
        }

        private String getNome() {
            return this.user;
        }

        private int getDemanda() {
            return this.demanda;
        }

        private Double getLimite() {
            return this.limiteConsumo;
        }

        private void setLimite(final Double lim) {
            this.limiteConsumo = lim;
        }

        private double getServedPower() {
            return this.servedPower;
        }

        private double getRelacaoEficienciSisPor() {
            return this.relacaoEficienciaSistemaPorcao;
        }

        //Comparador para ordenação
        @Override
        public int compareTo(final StatusUser o) {

            if (((this.servedPerf - this.perfShare) / this.perfShare) < ((o.getServedPerf() - o.getPerfShare()) / o.getPerfShare())) {
                return -1;
            }

            if (((this.servedPerf - this.perfShare) / this.perfShare) > ((o.getServedPerf() - o.getPerfShare()) / o.getPerfShare())) {
                return 1;
            }

            //Se as defasagens/excessos relativos forem iguais, os ifs anteriores não são executados
            if (this.perfShare > o.getPerfShare()) {
                return -1;
            } else {
                if (this.perfShare < o.getPerfShare()) {
                    return 1;
                } else {
                    if (this.powerShare >= o.getPowerShare()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            }
        }

        private double getPerfShare() {
            return this.perfShare;
        }

        private double getPowerShare() {
            return this.powerShare;
        }

        private void setPowerShare(final Double power) {
            this.powerShare = power;
        }

        private double getServedPerf() {
            return this.servedPerf;
        }
    }

    //Classe para arnazenar o estado das máquinas no sistema
    private static class ControleEscravos {
        private final String ID;//Id da máquina escravo
        private String status;//Estado da máquina
        private List<Tarefa> processador;

        private ControleEscravos(final String Ident, final List<Tarefa> P) {
            this.status = "Livre";
            this.ID = Ident;
            this.processador = P;
        }

        public String getID() {
            return this.ID;
        }

        private List<Tarefa> GetProcessador() {
            return this.processador;
        }

        private String getStatus() {
            return this.status;
        }

        private void setFila(final List<Tarefa> F) {
        }

        private void setProcessador(final List<Tarefa> P) {
            this.processador = P;
        }

        private void setOcupado() {
            this.status = "Ocupado";
        }

        private void setLivre() {
            this.status = "Livre";
        }

        private void setBloqueado() {
            this.status = "Bloqueado";
        }

        private void setIncerto() {
            this.status = "Incerto";
        }
    }

    //Classe para armazenar dados sobre as preempções que ainda não terminaram
    private static class ControlePreempcao {
        private final String usuarioPreemp;
        private final String usuarioAlloc;
        private final int preempID;//ID da tarefa que sofreu preempção
        private final int allocID;//ID da tarefa alocada

        private ControlePreempcao(final String userP, final int pID, final String userA,
                                  final int aID) {
            this.usuarioPreemp = userP;
            this.preempID = pID;
            this.usuarioAlloc = userA;
            this.allocID = aID;
        }

        private String getUsuarioPreemp() {
            return this.usuarioPreemp;
        }

        private int getPreempID() {
            return this.preempID;
        }

        private String getUsuarioAlloc() {
            return this.usuarioAlloc;
        }

        private int getAllocID() {
            return this.allocID;
        }
    }
}
