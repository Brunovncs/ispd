package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@ConcretePolicy
public class Workqueue extends GridSchedulingPolicy {
    private final LinkedList<Tarefa> ultimaTarefaConcluida = new LinkedList<>();
    private List<Tarefa> tarefaEnviada = null;

    public Workqueue() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.tarefaEnviada = new ArrayList<>(this.slaves.size());
        for (int i = 0; i < this.slaves.size(); i++) {
            this.tarefaEnviada.add(null);
        }
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public void schedule() {
        final CS_Processamento rec = this.scheduleResource();

        if (rec == null) {
            return;
        }

        final Tarefa trf = this.scheduleTask();

        if (trf == null) {
            return;
        }

        this.tarefaEnviada.set(this.slaves.indexOf(rec), trf);
        if (!this.ultimaTarefaConcluida.isEmpty()) {
            this.ultimaTarefaConcluida.removeLast();
        }
        trf.setLocalProcessamento(rec);
        trf.setCaminho(this.scheduleRoute(rec));
        this.policyMaster.sendTask(trf);
    }

    @Override
    public Tarefa scheduleTask() {
        if (!this.tasks.isEmpty()) {
            return this.tasks.remove(0);
        }
        return null;
    }

    @Override
    public void addCompletedTask(final Tarefa completedTask) {
        super.addCompletedTask(completedTask);
        this.ultimaTarefaConcluida.add(completedTask);
        if (!this.tasks.isEmpty()) {
            this.policyMaster.executeScheduling();
        }
    }

    @Override
    public void submitTask(final Tarefa newTask) {
        super.submitTask(newTask);
        if (this.tarefaEnviada.contains(newTask)) {
            final int index = this.tarefaEnviada.indexOf(newTask);
            this.tarefaEnviada.set(index, null);
            this.policyMaster.executeScheduling();
        }
    }

    @Override
    public CS_Processamento scheduleResource() {
        if (!this.ultimaTarefaConcluida.isEmpty() && !this.ultimaTarefaConcluida.getLast().isCopy()) {
            final int index =
                    this.tarefaEnviada.indexOf(this.ultimaTarefaConcluida.getLast());
            return this.slaves.get(index);
        } else {
            for (int i = 0; i < this.tarefaEnviada.size(); i++) {
                if (this.tarefaEnviada.get(i) == null) {
                    return this.slaves.get(i);
                }
            }
        }
        return null;
    }
}
