package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Implementation of the RoundRobin scheduling algorithm.<br> Hands over the next task on the FIFO
 * queue, for the next resource in a circular queue of resources.
 */
@ConcretePolicy
public class RoundRobin extends GridSchedulingPolicy {
    private ListIterator<CS_Processamento> resources = null;

    public RoundRobin() {
        this.tasks = new ArrayList<>(0);
        this.slaves = new LinkedList<>();
    }

    @Override
    public void initialize() {
        this.resources = this.slaves.listIterator(0);
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public void schedule() {
        final var task = this.scheduleTask();
        final var resource = this.scheduleResource();
        task.setLocalProcessamento(resource);
        task.setCaminho(this.scheduleRoute(resource));
        this.policyMaster.sendTask(task);
    }

    @Override
    public Tarefa scheduleTask() {
        return this.tasks.remove(0);
    }

    @Override
    public CS_Processamento scheduleResource() {
        if (!this.resources.hasNext()) {
            this.resources = this.slaves.listIterator(0);
        }
        return this.resources.next();
    }
}
