package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Mensagem;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyConditionSets;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.List;

@ConcretePolicy
public class M_OSEP extends GridSchedulingPolicy {
    private final List<ControleEscravos> controleEscravos = new ArrayList<>();
    private final List<Tarefa> esperaTarefas = new ArrayList<>();
    private final List<ControlePreempcao> controlePreempcao = new ArrayList<>();
    private final List<List<Tarefa>> processadorEscravos = new ArrayList<>();
    private Tarefa tarefaSelec = null;
    private List<StatusUser> status = null;
    private int contadorEscravos = 0;

    public M_OSEP() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.slavesTaskQueues = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.policyMaster.setSchedulingConditions(PolicyConditionSets.ALL);//Escalonamento quando
        // chegam tarefas e quando tarefas são concluídas
        this.status = new ArrayList<>();

        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {//Objetos de controle de
            // uso e cota para cada um dos usuários
            this.status.add(new StatusUser(this.userMetrics.getPoderComputacional(this.userMetrics.getUsuarios().get(i))));

        }

        for (int i = 0; i < this.slaves.size(); i++) {//Contadores para lidar com a dinamicidade
            // dos dados
            this.controleEscravos.add(new ControleEscravos());
            this.slavesTaskQueues.add(new ArrayList<>());
            this.processadorEscravos.add(new ArrayList<>());
        }
    }

    @Override
    public void schedule() {
        final Tarefa trf = this.scheduleTask();
        this.tarefaSelec = trf;
        if (trf != null) {
            final CS_Processamento rec = this.scheduleResource();
            if (rec != null) {
                trf.setLocalProcessamento(rec);
                trf.setCaminho(this.scheduleRoute(rec));
                //Verifica se não é caso de preempção
                if (this.controleEscravos.get(this.slaves.indexOf(rec)).Preemp()) {
                    final int index_rec = this.slaves.indexOf(rec);
                    this.esperaTarefas.add(trf);
                    this.controlePreempcao.add(new ControlePreempcao(this.processadorEscravos.get(index_rec).get(0).getProprietario(), (this.processadorEscravos.get(index_rec).get(0)).getIdentificador(), trf.getProprietario(), trf.getIdentificador()));
                    final int indexUser =
                            this.userMetrics.getUsuarios().indexOf((this.processadorEscravos.get(index_rec).get(0)).getProprietario());
                    this.status.get(indexUser).AtualizaUso(rec.getPoderComputacional(), 0);
                } else {
                    this.status.get(this.userMetrics.getUsuarios().indexOf(trf.getProprietario())).AtualizaUso(rec.getPoderComputacional(), 1);
                    this.policyMaster.sendTask(trf);
                }
            } else {
                this.tasks.add(trf);
                this.tarefaSelec = null;
            }
        }

    }

    @Override
    public Tarefa scheduleTask() {
        //Usuários com maior diferença entre uso e posse terão preferência
        double difUsuarioMinimo = -1;
        int indexUsuarioMinimo = -1;
        //Encontrar o usuário que está mais abaixo da sua propriedade
        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            //Verificar se existem tarefas do usuário corrente
            boolean demanda = false;

            for (final Tarefa task : this.tasks) {
                if (task.getProprietario().equals(this.userMetrics.getUsuarios().get(i))) {
                    demanda = true;
                    break;
                }
            }

            //Caso existam tarefas do usuário corrente e ele esteja com uso menor que sua posse
            if ((this.status.get(i).GetUso() < this.status.get(i).GetCota()) && demanda) {
                if (difUsuarioMinimo == (double) -1) {
                    difUsuarioMinimo = this.status.get(i).GetCota() - this.status.get(i).GetUso();
                    indexUsuarioMinimo = i;
                } else {
                    if (difUsuarioMinimo < this.status.get(i).GetCota() - this.status.get(i).GetUso()) {
                        difUsuarioMinimo =
                                this.status.get(i).GetCota() - this.status.get(i).GetUso();
                        indexUsuarioMinimo = i;
                    }
                }
            }
        }

        if (indexUsuarioMinimo != -1) {
            int indexTarefa = -1;

            for (int i = 0; i < this.tasks.size(); i++) {
                if (this.tasks.get(i).getProprietario().equals(this.userMetrics.getUsuarios().get(indexUsuarioMinimo))) {
                    if (indexTarefa == -1) {
                        indexTarefa = i;
                    } else {
                        if (this.tasks.get(indexTarefa).getTamProcessamento() > this.tasks.get(i).getTamProcessamento()) {
                            indexTarefa = i;
                        }
                    }
                }
            }

            if (indexTarefa != -1) {
                return this.tasks.remove(indexTarefa);
            }
        }

        if (this.tasks.isEmpty()) {
            return null;
        } else {
            return this.tasks.remove(0);
        }
    }

    @Override
    public void updateResult(final Mensagem mensagem) {
        super.updateResult(mensagem);
        final int index = this.slaves.indexOf(mensagem.getOrigem());
        this.processadorEscravos.set(index, mensagem.getProcessadorEscravo());
        this.contadorEscravos++;
        if (this.contadorEscravos == this.slaves.size()) {
            boolean escalona = false;
            for (int i = 0; i < this.slaves.size(); i++) {
                if (this.processadorEscravos.get(i).size() == 1 && !this.controleEscravos.get(i).Preemp()) {
                    this.controleEscravos.get(i).SetOcupado();
                } else if (this.processadorEscravos.get(i).isEmpty() && !this.controleEscravos.get(i).Preemp()) {
                    escalona = true;
                    this.controleEscravos.get(i).SetLivre();
                } else if (this.controleEscravos.get(i).Preemp()) {
                    this.controleEscravos.get(i).SetBloqueado();
                }
            }
            this.contadorEscravos = 0;
            if (!this.tasks.isEmpty() && escalona) {
                this.policyMaster.executeScheduling();
            }
        }
    }

    @Override
    public void submitTask(final Tarefa tarefa) {
        super.submitTask(tarefa);
        final CS_Processamento maq = (CS_Processamento) tarefa.getLocalProcessamento();
        final int indexUser;
        //Em caso de preempção, é procurada a tarefa correspondente para ser enviada ao escravo
        // agora desocupado
        if (tarefa.getLocalProcessamento() != null) {

            int j;
            int indexControle = -1;
            for (j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == tarefa.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(tarefa.getProprietario())) {
                    indexControle = j;
                    break;
                }
            }

            for (int i = 0; i < this.esperaTarefas.size(); i++) {
                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControle).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(j).getAllocID()) {
                    indexUser =
                            this.userMetrics.getUsuarios().indexOf(this.controlePreempcao.get(indexControle).getUsuarioAlloc());
                    this.status.get(indexUser).AtualizaUso(maq.getPoderComputacional(), 1);
                    this.policyMaster.sendTask(this.esperaTarefas.get(i));
                    //controleEscravos.get(index).SetBloqueado();
                    this.esperaTarefas.remove(i);
                    this.controlePreempcao.remove(j);
                    break;
                }
            }

        } else {
            this.policyMaster.executeScheduling();
        }
    }

    @Override
    public void addCompletedTask(final Tarefa tarefa) {
        super.addCompletedTask(tarefa);
        final CS_Processamento maq = (CS_Processamento) tarefa.getLocalProcessamento();
        final int indexUser = this.userMetrics.getUsuarios().indexOf(tarefa.getProprietario());
        this.status.get(indexUser).AtualizaUso(maq.getPoderComputacional(), 0);

        //System.out.println("Tarefa " + tarefa.getIdentificador() + " do user " + tarefa
        // .getProprietario() + " concluida " + mestre.getSimulacao().getTime() + " O usuário
        // perdeu " + maq.getPoderComputacional() + " MFLOPS");
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destino) {
        final int index = this.slaves.indexOf(destino);
        return new ArrayList<>(this.slaveRoutes.get(index));
    }

    @Override
    public CS_Processamento scheduleResource() {
        //Buscando recurso livre
        CS_Processamento selec = null;

        for (int i = 0; i < this.slaves.size(); i++) {
            if (this.slavesTaskQueues.get(i).isEmpty() && this.processadorEscravos.get(i).isEmpty() && this.controleEscravos.get(i).Livre()) {//Garantir que o escravo está de fato livre e que não há nenhuma tarefa em trânsito para o escravo
                if (selec == null) {
                    selec = this.slaves.get(i);
                } else if (Math.abs(this.slaves.get(i).getPoderComputacional() - this.tarefaSelec.getTamProcessamento()) < Math.abs(selec.getPoderComputacional() - this.tarefaSelec.getTamProcessamento())) {//Best Fit
                    selec = this.slaves.get(i);
                }
            }
        }

        if (selec != null) {
            this.controleEscravos.get(this.slaves.indexOf(selec)).SetBloqueado();//Inidcar que
            // uma tarefa será enviada e que , portanto , este escravo deve ser bloqueada até a
            // próxima atualização
            return selec;
        }

        String usermax = null;
        double diff = -1;

        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            if (this.status.get(i).GetUso() > this.status.get(i).GetCota() && !this.userMetrics.getUsuarios().get(i).equals(this.tarefaSelec.getProprietario())) {
                if (diff == (double) -1) {
                    usermax = this.userMetrics.getUsuarios().get(i);
                    diff = this.status.get(i).GetUso() - this.status.get(i).GetCota();
                } else {
                    if (this.status.get(i).GetUso() - this.status.get(i).GetCota() > diff) {
                        usermax = this.userMetrics.getUsuarios().get(i);
                        diff = this.status.get(i).GetUso() - this.status.get(i).GetCota();
                    }
                }
            }
        }

        int index = -1;
        if (usermax != null) {
            for (int i = 0; i < this.slaves.size(); i++) {
                if (this.processadorEscravos.get(i).size() == 1 && this.controleEscravos.get(i).Ocupado() && this.slavesTaskQueues.get(i).isEmpty() && ((Tarefa) this.processadorEscravos.get(i).get(0)).getProprietario().equals(usermax)) {
                    if (index == -1) {
                        index = i;
                    } else {
                        if (this.slaves.get(i).getPoderComputacional() < this.slaves.get(index).getPoderComputacional()) {
                            index = i;
                        }
                    }
                }
            }
        }

        //Fazer a preempção
        if (index != -1) {
            selec = this.slaves.get(index);
            //Verifica se vale apena fazer preempção
            int index_selec = this.slaves.indexOf(selec);
            final Tarefa tar = this.processadorEscravos.get(index_selec).get(0);

            final int indexUserEscravo =
                    this.userMetrics.getUsuarios().indexOf(tar.getProprietario());
            final int indexUserEspera =
                    this.userMetrics.getUsuarios().indexOf(this.tarefaSelec.getProprietario());

            //Penalidade do usuário dono da tarefa em execução, caso a preempção seja feita
            final double penalidaUserEscravoPosterior =
                    (this.status.get(indexUserEscravo).GetUso() - selec.getPoderComputacional() - this.status.get(indexUserEscravo).GetCota()) / this.status.get(indexUserEscravo).Cota;

            //Penalidade do usuário dono da tarefa slecionada para ser posta em execução, caso a
            // preempção seja feita
            final double penalidaUserEsperaPosterior =
                    (this.status.get(indexUserEspera).GetUso() + selec.getPoderComputacional() - this.status.get(indexUserEspera).GetCota()) / this.status.get(indexUserEspera).Cota;

            //Caso o usuário em espera apresente menor penalidade e os donos das tarefas em
            // execução e em espera não sejam a mesma pessoa , e , ainda, o escravo esteja
            // executando apenas uma tarefa
            if (penalidaUserEscravoPosterior <= penalidaUserEsperaPosterior || (penalidaUserEscravoPosterior > 0 && penalidaUserEsperaPosterior < 0)) {
                index_selec = this.slaves.indexOf(selec);
                this.controleEscravos.get(this.slaves.indexOf(selec)).setPreemp();
                this.policyMaster.sendMessage(this.processadorEscravos.get(index_selec).get(0),
                        selec, Mensagens.DEVOLVER_COM_PREEMPCAO);
                return selec;
            }
        }
        return null;
    }

    @Override
    public Double getUpdateInterval() {
        return 15.0;
    }

    private static class ControleEscravos {
        private int contador;

        private ControleEscravos() {
            this.contador = 0;
        }

        private boolean Ocupado() {
            return this.contador == 1;
        }

        private boolean Livre() {
            return this.contador == 0;
        }

        private boolean Preemp() {
            return this.contador == 3;
        }

        private void SetOcupado() {
            this.contador = 1;
        }

        private void SetLivre() {
            this.contador = 0;
        }

        private void SetBloqueado() {
            this.contador = 2;
        }

        private void setPreemp() {
            this.contador = 3;
        }
    }

    private static class ControlePreempcao {
        private final String usuarioPreemp;
        private final String usuarioAlloc;
        private final int preempID;
        private final int allocID;

        private ControlePreempcao(
                final String user1, final int pID,
                final String user2, final int aID) {
            this.usuarioPreemp = user1;
            this.preempID = pID;
            this.usuarioAlloc = user2;
            this.allocID = aID;
        }

        private String getUsuarioPreemp() {
            return this.usuarioPreemp;
        }

        private int getPreempID() {
            return this.preempID;
        }

        private String getUsuarioAlloc() {
            return this.usuarioAlloc;
        }

        private int getAllocID() {
            return this.allocID;
        }
    }

    private class StatusUser {
        private final Double Cota;
        private Double PoderEmUso;

        private StatusUser(final Double poder) {
            this.PoderEmUso = 0.0;
            this.Cota = poder;
        }

        private void AtualizaUso(final Double poder, final int opc) {
            if (opc == 1) {
                this.PoderEmUso = this.PoderEmUso + poder;
            } else {
                this.PoderEmUso = this.PoderEmUso - poder;
            }
        }

        private Double GetCota() {
            return this.Cota;
        }

        private Double GetUso() {
            return this.PoderEmUso;
        }
    }
}