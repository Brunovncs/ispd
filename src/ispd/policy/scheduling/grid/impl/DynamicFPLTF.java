package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.motor.filas.servidores.implementacao.CS_Maquina;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.List;

@ConcretePolicy
public class DynamicFPLTF extends GridSchedulingPolicy {
    private List<Double> tempoTornaDisponivel = null;
    private Tarefa tarefaSelecionada = null;

    public DynamicFPLTF() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.slavesTaskQueues = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.tempoTornaDisponivel = new ArrayList<>(this.slaves.size());
        for (int i = 0; i < this.slaves.size(); i++) {
            this.tempoTornaDisponivel.add(0.0);
            this.slavesTaskQueues.add(new ArrayList());
        }
    }

    @Override
    public Tarefa scheduleTask() {
        return this.tasks.remove(0);
    }

    @Override
    public CS_Processamento scheduleResource() {
        int index = 0;
        double menorTempo = this.slaves.get(index).tempoProcessar(
                this.tarefaSelecionada.getTamProcessamento());
        for (int i = 1; i < this.slaves.size(); i++) {
            final double tempoEscravoI = this.slaves.get(i).tempoProcessar(
                    this.tarefaSelecionada.getTamProcessamento());
            if (this.tempoTornaDisponivel.get(index) + menorTempo
                > this.tempoTornaDisponivel.get(i) + tempoEscravoI) {
                menorTempo = tempoEscravoI;
                index = i;
            }
        }
        return this.slaves.get(index);
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public void schedule() {
        final Tarefa trf = this.scheduleTask();
        this.tarefaSelecionada = trf;
        if (trf != null) {
            final CS_Processamento rec = this.scheduleResource();
            final int index = this.slaves.indexOf(rec);
            final double custo = rec.tempoProcessar(trf.getTamProcessamento());
            this.tempoTornaDisponivel.set(index,
                    this.tempoTornaDisponivel.get(index) + custo);
            trf.setLocalProcessamento(rec);
            trf.setCaminho(this.scheduleRoute(rec));
            this.policyMaster.sendTask(trf);
        }
    }

    @Override
    public void submitTask(final Tarefa newTask) {
        if (newTask.getOrigem().equals(this.policyMaster)) {
            this.userMetrics.incTarefasSubmetidas(newTask);
        }
        int k = 0;
        while (k < this.tasks.size() && this.tasks.get(k).getTamProcessamento() > newTask.getTamProcessamento()) {
            k++;
        }
        this.tasks.add(k, newTask);
    }

    @Override
    public void addCompletedTask(final Tarefa completedTask) {
        super.addCompletedTask(completedTask);
        final int index = this.slaves.indexOf(completedTask.getLocalProcessamento());
        if (index != -1) {
            final double custo =
                    this.slaves.get(index).tempoProcessar(completedTask.getTamProcessamento());
            if (this.tempoTornaDisponivel.get(index) - custo > 0) {
                this.tempoTornaDisponivel.set(index,
                        this.tempoTornaDisponivel.get(index) - custo);
            }
        }
        for (int i = 0; i < this.slaves.size(); i++) {
            if (this.slaves.get(i) instanceof CS_Maquina) {
                final CS_Processamento escravo = this.slaves.get(i);
                for (int j = 0; j < this.slavesTaskQueues.get(i).size(); j++) {
                    final Tarefa trf = (Tarefa) this.slavesTaskQueues.get(i).get(j);
                    final double custo =
                            escravo.tempoProcessar(trf.getTamProcessamento());
                    if (this.tempoTornaDisponivel.get(i) - custo > 0) {
                        this.tempoTornaDisponivel.set(i,
                                this.tempoTornaDisponivel.get(i) - custo);
                    }
                    this.policyMaster.sendMessage(trf, escravo,
                            Mensagens.DEVOLVER);
                }
                this.slavesTaskQueues.get(i).clear();
            }
        }
    }

    @Override
    public Double getUpdateInterval() {
        return 60.0;
    }
}
