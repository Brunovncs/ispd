package ispd.policy.scheduling.grid;

import ispd.policy.scheduling.SchedulingMaster;

/**
 * The {@code GridMaster} interface represents a class that can execute scheduling operations in a
 * grid-based computing system by communicating with the currently unfolding
 * {@link ispd.motor.Simulation Simulation}. It has no additional capabilities beyond
 * {@link SchedulingMaster}'s.
 * <p>
 * It extends the {@link SchedulingMaster} interface and adds no additional methods specific to
 * grid-based scheduling policies. However, it specifies that the {@link SchedulingMaster} it
 * represents is associated with a grid-based system.
 * <p>
 * This class has no further specializations. An example of a concrete implementation of this
 * interface is {@link ispd.motor.filas.servidores.implementacao.CS_Mestre CS_Mestre}.
 * <p>
 * This type of master is associated with the {@link GridSchedulingPolicy} policy type.
 *
 * @see SchedulingMaster The base interface for classes that can call methods on scheduling policies
 * @see GridSchedulingPolicy Policy type that uses this master type.
 * @see ispd.motor.filas.servidores.implementacao.CS_Mestre Concrete implementation of the interface
 */
public interface GridMaster extends SchedulingMaster {
}
