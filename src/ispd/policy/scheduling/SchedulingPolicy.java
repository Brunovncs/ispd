package ispd.policy.scheduling;

import ispd.motor.filas.Mensagem;
import ispd.motor.filas.Tarefa;
import ispd.motor.metricas.MetricasUsuarios;
import ispd.policy.Policy;

import java.util.List;

/**
 * {@code SchedulingPolicy} is an abstract specialization of {@link Policy} for the representation
 * of task scheduling algorithms.
 * <p>
 * It is parameterized with a type {@code T} that implements the {@link SchedulingMaster} interface,
 * and it provides methods that most scheduling policies will rely upon for their implementation.
 * <p>
 * This class does not represent a concrete, instantiatable {@link Policy} type. It has two further
 * specializations for grid and cloud-based computing systems:
 * {@link ispd.policy.scheduling.grid.GridSchedulingPolicy GridSchedulingPolicy} and
 * {@link ispd.policy.scheduling.cloud.CloudSchedulingPolicy CloudSchedulingPolicy} respectively.
 *
 * <hr>
 * Concrete subclasses of this class must provide an implementation for, besides the abstract
 * methods defined in {@link Policy}, the method {@link #scheduleTask()}. This method may be
 * implemented 'usefully' or not, depending on the scheduling policy. For further info, see the
 * documentation of the method: {@link #scheduleTask()}.
 *
 * <hr>
 * The initialization of scheduling policies follows the same guidelines as {@link Policy}'s, but
 * with further restrictions:
 * <ul>
 *     <li>Subclasses must initialize the fields {@link #tasks} and {@link #slavesTaskQueues}
 *     with appropriate collections.</li>
 *     <li>{@link #setUserMetrics} must be called with an exclusive instance of
 *     {@link MetricasUsuarios}.</li>
 * </ul>
 *
 * @param <T> the type of {@link SchedulingMaster} that this scheduling policy is associated with.
 *            It is expected that each scheduling policy type will have a corresponding
 *            specialization of {@link SchedulingMaster} to use.
 *
 * @see Policy The superclass this one specializes
 * @see SchedulingMaster Superclass of type parameter
 * @see ispd.policy.scheduling.grid.GridSchedulingPolicy Grid's specialization of this class
 * @see ispd.policy.scheduling.cloud.CloudSchedulingPolicy Cloud's specialization of this class
 */
public abstract class SchedulingPolicy <T extends SchedulingMaster> extends Policy<T> {
    /**
     * List of {@link Tarefa} (tasks) being managed by this policy.
     * <p>
     * When a scheduling policy instance is created, its value is {@code null}. Subclasses
     * <b>must</b> initialize this field with an appropriate collection on their constructors.
     * <p>
     * Tasks should then be added via the {@link #submitTask(Tarefa) submitTask} method.
     */
    protected List<Tarefa> tasks = null;
    /**
     * List of {@link Tarefa} (task) collections, representing the 'task queue's in each of the
     * {@link Policy#slaves slaves} this policy is responsible for.
     * <p>
     * This list should be kept in relative order with the field {@link Policy#slaves slaves}. That
     * is, the {@code k}-th list of {@link Tarefa tasks} in this list should represent the task list
     * of the {@code k}-th slave in the {@link Policy#slaves slaves list}.
     * <p>
     * When a scheduling policy instance is created, its value is {@code null}. Subclasses
     * <b>must</b> initialize this field with an appropriate collection (and a nested collection
     * for each slave) on their constructors.
     */
    protected List<List<Tarefa>> slavesTaskQueues = null;
    /**
     * {@link MetricasUsuarios} used to keep track of various user-related metrics, such as
     * ownership of computation power in the grid and energy consumption limits.
     * <p>
     * This field starts with the value {@code null}, and should be set with an appropriate instance
     * of {@link MetricasUsuarios} via the method
     * {@link #setUserMetrics(MetricasUsuarios) setUserMetrics}.
     */
    protected MetricasUsuarios userMetrics = null;

    /**
     * Schedules (selects) a task from the {@link #tasks list of tasks} to be executed in the
     * current scheduling cycle.
     * <p>
     * This method should only be used internally, within {@link #schedule()}'s implementation.
     * <p>
     * Concrete subclasses may choose to implement this method with a 'useless' implementation (such
     * as merely throwing an exception), if their task scheduling decision cannot be separated from
     * their scheduling cycle as a whole. For an example, see
     * {@link ispd.policy.scheduling.grid.impl.HOSEP#scheduleTask()} HOSEP's override}.
     *
     * @return the scheduled task as a {@link Tarefa} instance.
     */
    public abstract Tarefa scheduleTask();

    /**
     * Get the list of {@link Tarefa} (tasks) managed by this scheduling policy.
     *
     * @return the list of {@link Tarefa} (tasks) managed by this scheduling policy.
     */
    public List<Tarefa> getTasks() {
        return this.tasks;
    }

    /**
     * Get the {@link MetricasUsuarios} object associated with this scheduling policy instance.
     * <p>
     * This method will return the most recent metrics object associated with this instance via
     * {@link #setUserMetrics(MetricasUsuarios) setUserMetrics}, or {@code null} if none have been.
     *
     * @return the {@link MetricasUsuarios} object associated with this scheduling policy instance,
     * or {@code null} if there is none.
     */
    public MetricasUsuarios getUserMetrics() {
        return this.userMetrics;
    }

    /**
     * Set the {@link MetricasUsuarios} which should be used by this scheduling policy instance to
     * keep track of user metrics.
     * <p>
     * <b>Usage Note:</b>
     * <p>
     * This method <b>must</b> be called <i>strictly</i> before the scheduling policy is started,
     * otherwise a {@link NullPointerException} error will occur since the {@link #userMetrics}
     * fields starts with {@code null}.
     * <p>
     * Calling this method when the scheduling is already in execution will leave the scheduling
     * policy instance in an inconsistent state, which will lead to unpredictable behavior.
     * <p>
     * Calling this method with {@link MetricasUsuarios} instances already in use elsewhere will
     * lead to unpredictable behavior, most notably in the form of incorrect metrics values.
     *
     * @param newUserMetrics new {@link MetricasUsuarios} to be used by this scheduling policy
     *                       instance.
     *
     * @see MetricasUsuarios
     * @see #userMetrics
     * @see ispd.arquivo.xml.utils.UserPowerLimit#setSchedulerUserMetrics(SchedulingPolicy) Where
     * this method is used
     */
    public void setUserMetrics(final MetricasUsuarios newUserMetrics) {
        this.userMetrics = newUserMetrics;
    }

    /**
     * Update the {@link ispd.motor.filas.servidores.CS_Processamento CS_Processamento} (slave) set
     * as the origin of the given {@link Mensagem} (message).
     * <p>
     * More specifically, update the information pertaining to its
     * {@link #slavesTaskQueues task queue}, with information acquired from the
     * {@link Mensagem#getFilaEscravo() getFilaEscravo} method of {@link Mensagem}.
     *
     * @param message message with {@link #slavesTaskQueues task queue} information that needs to be
     *                updated in its origin.
     *
     * @throws IndexOutOfBoundsException if the message's origin is a
     *                                   {@link ispd.motor.filas.servidores.CS_Processamento
     *                                   CS_Processamento} (slave) which this scheduling policy
     *                                   instance is <b>not</b> responsible for.
     * @see Mensagem
     * @see Mensagem#getOrigem()
     * @see Mensagem#getFilaEscravo()
     * @see #slavesTaskQueues
     */
    public void updateResult(final Mensagem message) {
        final int index = this.slaves.indexOf(message.getOrigem());
        this.slavesTaskQueues.set(index, message.getFilaEscravo());
    }

    /**
     * Submit a new {@link Tarefa} (task) for this scheduling policy.
     * <p>
     * More specifically, add the given {@link Tarefa} to the internal list of tasks field,
     * {@link #tasks}. If the {@link Tarefa task}'s origin matches this policy's associated
     * {@link #policyMaster master}, update the {@link #userMetrics} with a task conclusion via its
     * {@link MetricasUsuarios#incTarefasSubmetidas(Tarefa) incTarefasSubmetidas} method.
     * <p>
     * Concrete scheduling policies may choose to override this method to update any specific
     * internal state they maintain when this event occurs.
     * <p>
     * Subclasses <b>should</b> call this method via a {@code super} call, or re-implement this
     * behavior, to keep user metrics up-to-date,
     *
     * @param newTask new {@link Tarefa} to be handled by this scheduling policy instance
     *
     * @see Tarefa#getOrigem()
     * @see MetricasUsuarios#incTarefasSubmetidas(Tarefa)
     */
    public void submitTask(final Tarefa newTask) {
        if (newTask.getOrigem().equals(this.policyMaster)) {
            this.userMetrics.incTarefasSubmetidas(newTask);
        }
        this.tasks.add(newTask);
    }

    /**
     * Update the scheduling policy's internal state with a {@link Tarefa} (task) completion.
     * <p>
     * More specifically, if the {@link Tarefa task}'s origin matches this policy's associated
     * {@link #policyMaster master}, update the {@link #userMetrics} with a task conclusion via its
     * {@link MetricasUsuarios#incTarefasConcluidas(Tarefa) incTarefasConcluidas} method.
     * <p>
     * Concrete scheduling policies may choose to override this method to update any specific
     * internal state they maintain when this event occurs. For example,
     * {@link ispd.policy.scheduling.grid.impl.Workqueue Workqueue}'s override updates its '<i>last
     * completed task</i>' field, and executes a scheduling cycle if there are any tasks left.
     * <p>
     * Subclasses <b>should</b> call this method via a {@code super} call, or re-implement this
     * behavior, to keep user metrics up-to-date,
     *
     * @param completedTask the concluded task.
     *
     * @see Tarefa#getOrigem()
     * @see MetricasUsuarios#incTarefasConcluidas(Tarefa)
     * @see ispd.policy.scheduling.grid.impl.Workqueue#addCompletedTask(Tarefa)
     */
    public void addCompletedTask(final Tarefa completedTask) {
        if (completedTask.getOrigem().equals(this.policyMaster)) {
            this.userMetrics.incTarefasConcluidas(completedTask);
        }
    }
}
