package ispd.policy;

import ispd.motor.Simulation;
import ispd.policy.vmallocation.VirtualMachineMaster;

/**
 * The {@code PolicyMaster} interface represents a class that may call a scheduling or allocation
 * {@link Policy}'s methods. The policy keeps a reference to its 'master' so it may use it to send
 * messages that may add events to the currently unfolding {@link Simulation}.
 * <p>
 * The interface contains two simple methods for fetching and updating a master's reference of the
 * {@link Simulation}.
 *
 * @see ispd.policy.scheduling.SchedulingMaster Scheduling policies' interface specialization
 * @see VirtualMachineMaster Vm allocation policies' interface specialization
 * @see ispd.motor.filas.servidores.implementacao.CS_Mestre Grid's concrete implementation of the
 * interface
 * @see ispd.motor.filas.servidores.implementacao.CS_VMM Cloud's concrete implementation of the
 * interface
 */
public interface PolicyMaster {
    /**
     * Returns the {@link Simulation} reference associated with this policy master.
     * <p>
     * The {@link Simulation} object returned will be the same object passed in most recent call to
     * {@link #setSimulation(Simulation) setSimulation} in this instance.
     * <p>
     * If {@link #setSimulation(Simulation) setSimulation} has not been called in this instance, or
     * if it has been called iwth a {@code null} argument, this method will return {@code null}.
     *
     * @return the {@link Simulation} associated with this policy master, or {@code null} if none
     * has been set.
     */
    Simulation getSimulation();

    /**
     * Updates this policy master's {@link Simulation} reference with a new one.
     * <p>
     * Callers of this method should:
     * <ul>
     *     <li>only call this method <b>once</b> per {@code PolicyMaster} instance.</li>
     *     <li><b>not</b> call this method with {@link Simulation} instances that have already
     *     started running.</li>
     * </ul>
     * Violating either of these restrictions will produce unpredictable behavior.
     * <p>
     * To see how this method is utilized in practice, see concrete {@link Simulation} classes such
     * as {@link ispd.motor.SimulacaoSequencial} and {@link ispd.motor.SimulacaoParalela}.
     *
     * @param newSimulation the new {@link Simulation} to be associated with this policy master.
     */
    void setSimulation(Simulation newSimulation);
}
