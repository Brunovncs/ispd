package ispd.policy;

import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;

import java.util.List;

/**
 * The {@code Policy} abstract class represents a scheduling or allocation policy in the system.
 * <p>
 * It is parameterized with a type {@code T} that implements the {@link PolicyMaster} interface, and
 * it provides methods that most policies will rely upon for their implementation.
 * <p>
 * It also provides getters and setters for fields that are common to all policies, such as the
 * {@link PolicyMaster} instance associated with the policy, and lists of slaves and slave routes.
 *
 * <hr>
 * Concrete subclasses of this class must provide implementations for the
 * {@link #initialize() initialize}, {@link #schedule() schedule} and
 * {@link #scheduleRoute(CentroServico) scheduleRoute} methods. These methods are used to initialize
 * the internal state of the executing policy and execute a policy cycle.
 * <p>
 * The method {@link #scheduleResource()} may be implemented 'usefully' or not, depending on the
 * scheduling policy. For further info, see the documentation of the method:
 * {@link #scheduleResource()}.
 *
 * <hr>
 * Before a policy can do any meaningful work, it must be initialized correctly. Doing so involves
 * several restrictions:
 * <ul>
 *     <li>Subclasses must initialize the field {@link #slaves} with an appropriate collection in
 *     their constructors.</li>
 *     <li>{@link #setPolicyMaster} must be called with an appropriate master type for the policy
 *     specialization.</li>
 *     <li>{@link #addSlave} must be called to add each slave that this policy's master is
 *     responsible for.</li>
 *     <li>Lastly, {@link #updateSlaveRoutes} must be called with valid routes for all the slaves
 *     added.</li>
 *     <li>The method {@link #initialize()} must be called. Depending on the policy
 *     implementation, it may be responsible for covering some of the requisites above.</li>
 * </ul>
 *
 * @param <T> the type of {@link PolicyMaster} that this policy is associated with. It is expected
 *            that each policy type will have a corresponding specialization of {@link PolicyMaster}
 *            to use.
 *
 * @see PolicyMaster Superclass of type parameter
 * @see ispd.policy.scheduling.SchedulingPolicy Scheduling policies' specialization
 * @see ispd.policy.vmallocation.VmAllocationPolicy Vm allocation policies' specialization
 */
public abstract class Policy <T extends PolicyMaster> {
    /**
     * The {@link PolicyMaster} instance associated with this policy.
     * <p>
     * When the policy instance is created, its value is {@code null}. It can be set to a valid
     * reference to a {@link PolicyMaster} using the
     * {@link #setPolicyMaster(PolicyMaster) setPolicyMaster} method.
     *
     * @see #setPolicyMaster(PolicyMaster) setPolicyMaster
     */
    protected T policyMaster = null;
    /**
     * A list of {@link CS_Processamento} slaves which the {@link #policyMaster} associated with
     * this policy has access to / is responsible for.
     * <p>
     * When a policy instance is created, its value is {@code null}. Subclasses <b>must</b>
     * initialize this field with an appropriate collection on their constructors.
     * <p>
     * To populate the list, the method {@link #addSlave(CS_Processamento) addSlave} is used.
     *
     * @see #initialize()
     * @see #addSlave(CS_Processamento)
     */
    protected List<CS_Processamento> slaves = null;
    /**
     * A list of routes to the {@link #slaves}. See {@link #updateSlaveRoutes(List)} for
     * "invariants" of this field.
     * <p>
     * When the policy instance is created, its value is {@code null}. The method
     * {@link #updateSlaveRoutes(List) updateSlaveRoutes} is used to set the value of the field to
     * an appropriate list containing all needed routes.
     *
     * @see #updateSlaveRoutes(List) updateSlaveRoutes
     */
    protected List<List<CentroServico>> slaveRoutes = null;

    /**
     * Initializes the policy's internal state.
     * <p>
     * This method <i>must</i> be called before executing any policy cycles with the method
     * {@link #schedule() schedule}. Otherwise, unpredictable behavior will occur. Most likely, the
     * program will end with a {@link NullPointerException}.
     */
    public abstract void initialize();

    /**
     * Executes a policy cycle.
     * <p>
     * If this method is called before the policy is initialized correctly (as described in the
     * class' javadoc), unpredictable behavior will occur. Most likely, the program will end with a
     * {@link NullPointerException}.
     */
    public abstract void schedule();

    /**
     * Schedules a route for a given destination, starting at the {@link #policyMaster}.
     *
     * @param destination destination for which a route to is desired.
     *
     * @return the scheduled route as a list of {@link CentroServico} instances.
     */
    public abstract List<CentroServico> scheduleRoute(CentroServico destination);

    /**
     * Schedules a resource to execute a task (for task scheduling policies), or to host a virtual
     * machine (for vm allocation policies).
     * <p>
     * This method should only be used internally, within {@link #schedule()}'s implementation.
     * <p>
     * Concrete subclasses may choose to implement this method with a 'useless' implementation (such
     * as merely throwing an exception), if their resource scheduling decision cannot be separated
     * from their scheduling cycle as a whole. For example, see
     * {@link ispd.policy.scheduling.grid.impl.HOSEP HOSEP}'s override.
     *
     * @return the scheduled resource as a {@link CS_Processamento} instance.
     */
    public abstract CS_Processamento scheduleResource();

    /**
     * Get the {@link PolicyMaster} instance associated with this policy. Will return {@code null}
     * if the method {@link #setPolicyMaster(PolicyMaster) setPolicyMaster} has not been called on
     * this policy instance yet.
     *
     * @return the {@link PolicyMaster} instance associated with this policy, or {@code null} if one
     * hasn't been set yet.
     *
     * @see #policyMaster
     */
    public T getPolicyMaster() {
        return this.policyMaster;
    }

    /**
     * Set the {@link PolicyMaster} instance associated with this policy.
     * <p>
     * This method should be called only <i>once</i>, exactly during the scheduling policy's
     * initialization (as described in the class' javadoc).
     * <p>
     * Calling this method more than once, or at an inopportune time, or with an invalid
     * {@link PolicyMaster} reference will cause unexpected behavior to occur.
     *
     * @param newPolicyMaster the new {@link PolicyMaster} to be associated with this policy.
     *
     * @see #policyMaster
     */
    public void setPolicyMaster(final T newPolicyMaster) {
        this.policyMaster = newPolicyMaster;
    }

    /**
     * Get the list of {@link CS_Processamento} slaves which the {@link #policyMaster} associated
     * with this policy has access to / is responsible for.
     *
     * @return the list of slaves, as a list of {@link CS_Processamento}.
     *
     * @see #slaves
     */
    public List<CS_Processamento> getSlaves() {
        return this.slaves;
    }

    /**
     * Add a new slave to the {@link #slaves list of slaves} for this policy.
     *
     * @param newSlave {@link CS_Processamento} to add to the slave list.
     *
     * @see #slaves
     */
    public void addSlave(final CS_Processamento newSlave) {
        this.slaves.add(newSlave);
    }

    /**
     * Set the list of slave routes to be used in the scheduling cycles. The list passed should be
     * the same length as the {@link #slaves list of slaves}, and their relative order should be the
     * same. (That is, the route for the slave in the {@code k}-th index in the slaves list should
     * be at the {@code k}-th index in the route list.)
     * <p>
     * This method should be called during the policy's initialization (as described in the class'
     * javadoc). However, in contrast to {@link #setPolicyMaster(PolicyMaster) setPolicyMaster},
     * this method may <b>validly</b> be called <i>more than once</i>. For instance, to update
     * routing information after a previously used link between the master and its slave has
     * developed a fault.
     *
     * @param newSlaveRoutes the new list of routes to be used.
     *
     * @see #slaves
     * @see #slaveRoutes
     */
    public void updateSlaveRoutes(final List<List<CentroServico>> newSlaveRoutes) {
        this.slaveRoutes = newSlaveRoutes;
    }

    /**
     * Returns the time interval at which this policy should be updated, or {@code null} if such
     * update condition is not applicable to this policy.
     *
     * @return the update interval for this policy, or {@code null} if the policy does not require
     * updates at regular intervals.
     */
    public Double getUpdateInterval() {
        return null;
    }
}
