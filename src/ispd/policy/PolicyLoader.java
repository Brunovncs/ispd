package ispd.policy;

/**
 * An interface for classes that can load/create {@link Policy} instances.
 *
 * @param <T> the type of {@link Policy} objects that this loader can load. It is expected that each
 *            policy type will have a specialization of this interface to use.
 *
 * @see ispd.policy.loaders.GenericPolicyLoader Generic abstract policy loader
 * @see ispd.policy.loaders.GridSchedulingPolicyLoader Concrete grid scheduling policy loader
 * @see ispd.policy.loaders.CloudSchedulingPolicyLoader Concrete cloud scheduling policy loader
 * @see ispd.policy.loaders.VmAllocationPolicyLoader Concrete vm allocation policy loader
 */
public interface PolicyLoader <T extends Policy<?>> {
    /**
     * Loads and instances a {@link Policy} object of the policy with given name.
     * <p>
     * Concrete implementations of this method may opt to throw a {@link RuntimeException} on
     * failure.
     *
     * @param policyName the name of the {@link Policy} to be loaded and instantiated.
     *
     * @return the instantiated {@link Policy} object.
     */
    T loadPolicy(String policyName);
}
