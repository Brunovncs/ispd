/**
 * This package contains classes and subpackages related to task and resource scheduling, and vm
 * allocation policies. It also contains utilities for loading and managing new and existing
 * policies within the system.
 *
 * <hr>
 * <p>
 * The {@link ispd.policy.PolicyMaster PolicyMaster} interface contains methods that a master
 * associated with any type of policy must implement.
 * {@link ispd.policy.scheduling.SchedulingMaster SchedulingMaster},
 * {@link ispd.policy.vmallocation.VirtualMachineMaster VirtualMachineMaster},
 * {@link ispd.policy.scheduling.cloud.CloudMaster CloudMaster}, and
 * {@link ispd.policy.scheduling.grid.GridMaster GridMaster} are all specializations of this
 * interface.
 * <p>
 * The {@link ispd.policy.Policy Policy} abstract class contains fields and methods that all
 * policies rely upon. {@link ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy},
 * {@link ispd.policy.vmallocation.VmAllocationPolicy VmAllocationPolicy},
 * {@link ispd.policy.scheduling.cloud.CloudSchedulingPolicy CloudSchedulingPolicy}, and
 * {@link ispd.policy.scheduling.grid.GridSchedulingPolicy GridSchedulingPolicy} are all
 * specializations of this abstract class. Each type of {@link ispd.policy.Policy Policy} uses its
 * own type of {@link ispd.policy.PolicyMaster PolicyMaster}.
 * <p>
 * It is important to note that {@link ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy} and
 * {@link ispd.policy.scheduling.SchedulingMaster SchedulingMaster} are merely intermediate classes
 * (that are later specialized into a grid and cloud version), and are not intended to represent a
 * concrete, instantiatable type of policy and master respectively.
 * <p>
 * The {@link ispd.policy.PolicyCondition PolicyCondition} is an enum representing conditions in
 * which a policy "cycle" may be executed, and the
 * {@link ispd.policy.PolicyConditionSets PolicyConditionSets} class has convenience static fields
 * for subsets of all conditions.
 *
 * <hr>
 * <p>
 * The {@link ispd.policy.loaders} subpackage hosts <i>policy loaders</i>. Loaders are responsible
 * for mapping policy names into instantiated objects that can be used in simulations. All loaders
 * follow the {@link ispd.policy.PolicyLoader PolicyLoader} interface.
 * <p>
 * The {@link ispd.policy.managers} subpackage hosts <i>policy managers</i>. Managers have a wide
 * range of responsibilities, including: keeping track of which policies were added and removed
 * during program execution, compiling source code and saving bytecode of newly generated policies,
 * and extracting policies from jar files. All managers follow the
 * {@link ispd.policy.PolicyManager PolicyManager} interface.
 * <p>
 * Loaders and managers have both a 'generic' version
 * ({@link ispd.policy.loaders.GenericPolicyLoader GenericPolicyLoader} and
 * {@link ispd.policy.managers.FilePolicyManager FilePolicyManager} respectively), and
 * specializations for each type of policy.
 *
 * <hr>
 * <p>
 * Within the subpackages {@link ispd.policy.scheduling} and {@link ispd.policy.vmallocation}, there
 * are:
 * <ul>
 *     <li>Appropriate specializations for the {@link ispd.policy.Policy Policy} abstract class
 *     and the {@link ispd.policy.PolicyMaster PolicyMaster} interface.</li>
 *     <li>A subpackage <i>impl</i>, which hosts concrete native implementations for
 *     corresponding policy type of the subpackage.</li>
 *     <li>(Optionally) Within the <i>impl</i> subpackage, a <i>util</i> subdirectory to host
 *     utility classes used in the policy implementations.</li>
 * </ul>
 * Furthermore, the {@link ispd.policy.scheduling scheduling} subpackage is further subdivided
 * into a {@link ispd.policy.scheduling.grid} and {@link ispd.policy.scheduling.cloud}
 * subpackages, that follow the above-mentioned package structure.
 */
package ispd.policy;