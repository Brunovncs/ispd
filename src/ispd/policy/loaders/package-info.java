/**
 * This package contains classes that provide a way to load {@link ispd.policy.Policy Policy}
 * implementations by their names.
 *
 * <hr>
 * <p>
 * The {@link ispd.policy.loaders.GenericPolicyLoader GenericPolicyLoader} class is an abstract
 * class that provides a method for loading and instantiating policy objects using reflection. It
 * has three concrete implementations, namely
 * {@link ispd.policy.loaders.GridSchedulingPolicyLoader GridSchedulingPolicyLoader},
 * {@link ispd.policy.loaders.CloudSchedulingPolicyLoader CloudSchedulingPolicyLoader} and
 * {@link ispd.policy.loaders.VmAllocationPolicyLoader VmAllocationPolicyLoader}, which all
 * specialize the generic policy loader class for their respective policy types.
 *
 * <hr>
 * <p>
 * To load a policy, create an instance of the appropriate policy loader and call its
 * {@link ispd.policy.loaders.GenericPolicyLoader#loadPolicy(String) loadPolicy} method, passing in
 * the name of the policy class as a string. The policy will be instantiated using reflection and
 * its default constructor.
 */
package ispd.policy.loaders;