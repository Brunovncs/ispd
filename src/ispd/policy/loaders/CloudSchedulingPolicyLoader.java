package ispd.policy.loaders;

import ispd.policy.scheduling.cloud.CloudSchedulingPolicy;

/**
 * A concrete implementation of the {@link GenericPolicyLoader} class for policies of type
 * {@link CloudSchedulingPolicy}.
 * <p>
 * This class overrides the method {@link #getPolicyPackagePath()} to return the path for cloud
 * scheduling policies, that being {@value #CLASS_PATH}.
 *
 * @see GenericPolicyLoader
 * @see CloudSchedulingPolicy
 */
public class CloudSchedulingPolicyLoader extends GenericPolicyLoader<CloudSchedulingPolicy> {
    /**
     * The package path of cloud scheduling policy implementations.
     */
    private static final String CLASS_PATH = "ispd.policy.scheduling.cloud.impl.";

    /**
     * Returns the package path for cloud scheduling policy implementations.
     *
     * @return the package path for cloud scheduling policies as a string.
     */
    @Override
    protected String getPolicyPackagePath() {
        return CloudSchedulingPolicyLoader.CLASS_PATH;
    }
}
